#using HelperUtils


struct ArbPair
    id::Int64
    val::Float64
end
value(ap::ArbPair) = ap.val
id(ap::ArbPair) = ap.id

abstract type Order end

struct MarketOrder <: Order
    feed::Symbol
    pair::Tuple{Symbol, Symbol}
    side::Symbol
    amount::Float64
    price::Float64
    quote_lag::Float64
    time_received::Float64
    arb::Union{ArbPair, Vector{ArbPair}}
    hash::UInt64
end

struct LimitOrder <: Order
    feed::Symbol
    pair::Tuple{Symbol, Symbol}
    side::Symbol
    amount::Float64
    price::Float64
    quote_lag::Float64
    time_received::Float64
    arb::Union{ArbPair, Vector{ArbPair}}
    hash::UInt64
end

struct Tick
    feed::Symbol
    pair::Tuple{Symbol,Symbol}
    bid::Float64
    bid_volume::Float64
    ask::Float64
    ask_volume::Float64
    timestamp::Float64
    time_received::Float64
    flags
end

#=
function Order(o::T; kargs...) where T <: Order

end
=#

function size_check(o::Order, size_dict::Dict{Symbol, Dict{String, Float64}})
    @assert order.price >= size_dict[order.pair[1]]["min_size"]
end

function precision_check(order::T, size_dict::Dict{Tuple{Symbol,Symbol}, Dict{String, Real}}) where { T <: Order}
    ss = size_dict[order.pair]
    ap = ss["amt_precision"]
    pp = ss["price_precision"]
    new_amount = order.side == :sell ? floor(order.amount, digits=ap) : ceil(order.amount, digits=ap)
    new_price = order.side == :buy ? floor(order.price, digits=pp) : ceil(order.price, digits=pp)
    if "step_size" in keys(ss)   # fix for binanceus
        ii = -1 * log10(ss["step_size"]) |> Int
        new_amount = round(new_amount, digits=ii)
        try
            @assert new_amount > ss["min_quantity"]
            #@assert typeof(Int(new_amount * 10^ii)) == Int
            return T(
                order.feed,
                order.pair,
                order.side,
                new_amount,
                new_price,
                order.quote_lag,
                order.time_received,
                order.arb,
                order.hash
            )
        catch e
            return e
        end
    else
        return T(
            order.feed,
            order.pair,
            order.side,
            new_amount,
            new_price,
            order.quote_lag,
            order.time_received,
            order.arb,
            order.hash
        )
    end
end

function process_tick(tick)
    feed = tick["feed"] |> lowercase |> Symbol
    feed = feed == :coinbase ? :coinbasepro : feed
    feed = feed == :binance_us ? :binanceus : feed
    sym1, sym2 = map(Symbol, split(tick["pair"],"-"))
    flags = get(tick,"flags",[])
    flags = map(Symbol, flags)
    Tick(
        feed,
        (sym1, sym2),
        tick["bid"] |> safe_parse_F64,
        tick["bid_volume"] |> safe_parse_F64,
        tick["ask"] |> safe_parse_F64,
        tick["ask_volume"] |> safe_parse_F64,
        tick["timestamp"] |> safe_parse_F64,
        tick["time_received"] |> safe_parse_F64,
        flags
    )
end

function any_orders_stale(orders::Array{Order,1}, max_lag::Real)
    test_results = map(orders) do order
        max_lag < order.quote_lag
    end
    true in test_results
end

function any_orders_stale(dic::Dict{Symbol, Any}, max_lag::Real)
    any_orders_stale(dic[:orders], max_lag)
end
