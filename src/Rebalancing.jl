function find_by_denom(denom::Symbol, pa::Array{Tuple{Symbol,Symbol},1})
    out = Array{Symbol,1}()
    for (n,d) in pa
        if d == denom
            push!(out, n)
        end
    end
    out
end


function find_by_denom(denom::Symbol, pd::Dict{Symbol, Array{Tuple{Symbol,Symbol},1}})
    res = map(x-> find_by_denom(denom, x), values(pd))
    foldl(vcat, res) |> Set
end


function refresh_price(order::LimitOrder, f::Feather, cg::ConstGraph, l::ReentrantLock)
    nprice,tlog = lookup_price(order.pair, order.side, order.feed, f, cg, l, true)
    LimitOrder(
        order.feed,
        order.pair,
        order.side,
        order.amount,
        nprice,
        time() - tlog,
        tlog,
        order.arb_id,
        order.arb_val,
        hash(time(), order.hash)
    )
end

function get_pairs(cg::ConstGraph)
    s = foldl(vcat,collect(values(cg.pairs_dict))) |> union
    sort!(s, by=x->x[2])
end

function get_pairs(ename::Symbol, cg::ConstGraph)
    s = cg.pairs_dict[ename]
    sort!(s, by=x->x[2])
end

function filter_convert_pairs(ar::Array{Tuple{Symbol,Symbol},1}, denom_order = [:USD, :BTC, :ETH])
    nums = [x[1] for x in ar] |> union
    out = Array{Tuple{Symbol,Symbol},1}()
    for n in nums
        cand = Array{Tuple{Symbol,Symbol},1}()
        for d in denom_order
            if (n,d) in ar
                push!(cand, (n,d))
            end
        end
        push!(out, cand[1])
    end
    out
end

function filter_convert_pairs(ename::Symbol, cg::ConstGraph, denom_order = [:USD, :BTC, :ETH])
    ar = get_pairs(ename, cg)
    filter_convert_pairs(ar, denom_order)
end


function get_exchanges(pair::Tuple{Symbol,Symbol}, cg::ConstGraph)
    out = Vector{Symbol}()
    for (k,v) in cg.pairs_dict
        if pair in v
            push!(out, k)
        end
    end
    out |> sort
end

function convert_balances(bdic::Dict{Symbol, Float64}, ename::Symbol, f::Feather, cg::FixedGraph, l::ReentrantLock)
    dpd = get_common_prices(ename, f, cg, l, :USD)
    convert_balances(dpd, bdic, ename, f, cg, l)
end

function convert_balances(dpd::Dict, bdic::Dict{Symbol, Float64}, ename::Symbol, f::Feather, cg::FixedGraph, l::ReentrantLock)
    dollar_prices = Dict(k=>v() for (k,v) in dpd)
    out = Dict{Symbol, Tuple{Float64,Symbol}}()
    for (cur, price) in dollar_prices
        amt = get(bdic, cur, 0.0)
        out[cur] = (amt * price, :USD)
    end
    return out
end

function convert_balances(bb::Dict{Symbol, Dict{Symbol, Float64}}, f::Feather, cg::FixedGraph, l::ReentrantLock; aliases=Dict())
    out = Dict{Symbol, Dict{Symbol,Tuple{Float64,Symbol}}}()
    for (k_nominal, balances) in bb
        k = Base.get(aliases, k_nominal, k_nominal)
        if k in keys(cg.pairs_dict)
            convert_dict = common_price_func(k, f, cg, l, :USD)
            out[k_nominal] = Dict{Symbol, Tuple{Float64,Symbol}}()
            for (cur, amt) in balances
                out[k_nominal][cur] = (convert_dict(cur) * amt, :USD)
            end
        end
    end
    out
end

function percentage_total(ak::Dict{Symbol, APIKey}, f::Feather, cg::FixedGraph, l::ReentrantLock, denom_order=[:USD,:BTC,:ETH]; aliases::Dict{Symbol,Symbol}=Dict{Symbol,Symbol}())
    bb = getClientBalances(ak; aliases=aliases)
    percentage_total(bb, f, cg, l, denom_order; aliases=aliases)
end

function percentage_total(bb::Dict{Symbol,Dict{Symbol,Float64}}, f::Feather, cg::FixedGraph, l::ReentrantLock, denom_order::Array{Symbol,1}; aliases=Dict())
    ab = convert_balances(bb, f, cg, l; aliases=aliases)
    c_cur = denom_order[1]
    cc = map(dic -> dic |> keys |> collect, values(ab)) |> y->foldl(vcat,y) |> union |> sort
    total_by_cur = Dict{Symbol, Float64}()
    total_by_exch = Dict{Symbol, Float64}()
    units = Dict{Symbol,Dict{Symbol, Float64}}()
    for cur in cc
        for (k,dict) in ab
            t_run = get(total_by_cur, cur, 0.0)
            amt, cur_calc  = get(dict, cur, (0.0, c_cur))
            @assert cur_calc == c_cur
            total_by_cur[cur] = t_run + amt
            e_run = get(total_by_exch, k, 0.0)
            total_by_exch[k] = e_run + amt
            udic = get(units, cur, Dict{Symbol, Float64}())
            udic[k] = get(bb[k], cur, 0.0)
            units[cur] = udic
        end
    end
    total_perc = total_by_cur |> totals_to_percs
    exch_perc =total_by_exch |> totals_to_percs
    ep = Dict{Symbol, Dict{Symbol, Tuple{Float64,Float64, Float64}}}()
    for (k,ta) in total_by_cur
        kdic = get(ep, k, Dict{Symbol,Tuple{Float64,Float64,Float64}}())
        for (ename, dic) in ab
            a,_ = get(dic, k, (0.0,0.0))
            b = get(bb[ename], k, 0.0)
            adivta = b == 0.0 ? 0.0 : a / ta
            kdic[ename] = (a, adivta, b)
        end
        ep[k] = kdic
    end

    total_units = Dict{Symbol, Float64}()
    for (cur, dic) in units
        total_units[cur] =  dic |> values |> sum
    end

    out = Dict(
        :total => total_by_cur |> values |> sum,
        :total_currency => total_by_cur,
        :percent_currency => total_perc,
        :total_exchange => total_by_exch,
        :percent_exchange => exch_perc,
        :full => ep,
        :units_by_currency => units,
        :units_by_exchange => bb,
        :total_units => total_units
    )
end

function show_rebalance(ak::Dict{Symbol, APIKey}, f::Feather, cg::FixedGraph, l::ReentrantLock, denom_order=[:USD])
    pt = percentage_total(ak,f,cg,l,denom_order)
    show_rebalance(pt, cg)
end

function br(l::Int64=25, c::String="-")
    o = ""
    for i=1:l
        o = o*c
    end
    println(o)
end

br(c::String) = br(25,c)
br(c::Char) = br(25,string(c))
br(l::Int64, c::Char) = br(l, string(c))

function sp(n::Int)
    out = ""
    if n > 0
        for i=1:n
            out = out * " "
        end
    end
    out
end

function show_rebalance(ename::Symbol, pt::Dict, cg::ConstGraph)
    tv = pt[:total_exchange][ename]
    tvs = round(tv,digits=2) |> string
    s_length = 32 + length(string(ename))
    br(s_length)
    println("|  Total value at ", ename, " is: \$", tvs, "  |")
    br(s_length)

    oo = pt[:units_by_exchange][ename]
    br(54,'*')
    for (cur, amt) in oo
        try
            #a = round(amt, digits=2) |> string
            a = @sprintf "%.2f" amt
            #p = round(pt[:percent_currency][cur]*100,digits=1) |> string
            pp = pt[:full][cur][ename][1] / pt[:total_exchange][:coinbasepro]
            p = @sprintf "%.1f" (pp*100)
            ss1 = sp(length(tvs) + 3 - length(a))
            ss2 = sp(7- length(p))
            println(cur, sp(3),"|", ss1, a, sp(3), "|", ss2,  p,"%")
        catch
            println("No ", cur)
        end
    end
    br(54,'*')
end

function show_rebalance(pt::Dict, cg::FixedGraph)
    tv = pt[:total]
    tvs = @sprintf "%.2f" tv
    br(28)
    println("|  Total value is: \$", tvs, "  |")
    br(28)
    oo = sort(pt[:total_currency] |> collect, by=x->x[2], rev=true)
    for (cur, amt) in oo
        tunits = pt[:total_units][cur]
        tu = unit_round(tunits, cur)
        br(54)
        #a = round(amt, digits=2) |> string
        a = @sprintf "%.2f" amt
        #p = round(pt[:percent_currency][cur]*100,digits=1) |> string
        p = @sprintf "%.1f" (pt[:percent_currency][cur]*100)
        ss1 = sp(length(tvs) + 1 - length(a))
        ss2 = sp(7- length(p))
        println(cur, sp(3), "|", sp(3), tu, sp(3), "|", sp(3), "\$", ss1, a, sp(3), "|", ss2,  p,"% of Total")

        dict = pt[:full][cur]
        ooo = sort(collect(dict), by=x->x[2][1], rev=true)
        br(54,'*')
        for (ename, pp) in ooo
            if cur in keys(cg.cur_exch)
                if ename in cg.cur_exch[cur]
                    es = string(ename)
                    amt, perc = pp
                    uts = pt[:units_by_currency][cur][ename]
                    utr = unit_round(uts, cur) |> string
                    ssu = sp(9 - length(utr))
                    ss1 = sp(15 - length(es))
                    a = @sprintf "%.2f" amt
                    ss2 = sp(length(tvs) + 1 - length(a))
                    p = @sprintf "%.1f" (perc*100)
                    ss3 = sp(7 - length(p))
                    println(cur, ":", es, ss1, "|", sp(3), utr, ssu, "|", sp(3), "\$", ss2, a, sp(3), "|", ss3, p, "%")
                end
            else
                println(" ** No information for holdings of ", cur, " ** ")
            end
        end
        br(54,'*')
    end
    br('=')

    println()
    println("Total value by exchange is:")
    oe = sort(pt[:total_exchange] |> collect, by=x->x[2], rev=true)
    for (ename, amt) in oe
        es = string(ename)
        ss1 = sp(15 - length(es))
        a = @sprintf "%.2f" amt
        ss2 = sp(length(tvs) + 1 - length(a))
        p = @sprintf "%.1f" (pt[:percent_exchange][ename]*100)
        ss3 = sp(7 - length(p))
        println(es, ss1, "|", sp(3), "\$", ss2, a, sp(3), "|", ss3, p, "%")
    end
    br('=')

    println()
    println("Total value by currency is:")
    oo = sort(pt[:total_currency] |> collect, by=x->x[2], rev=true)
    for (cur, amt) in oo
        #a = round(amt, digits=2) |> string
        a = @sprintf "%.2f" amt
        #p = round(pt[:percent_currency][cur]*100,digits=1) |> string
        p = @sprintf "%.1f" (pt[:percent_currency][cur]*100)
        utot = pt[:total_units][cur]
        utr = unit_round(utot, cur) |> string
        ss1 = sp(length(tvs) + 1 - length(a))
        ss2 = sp(7- length(p))
        ssu = sp(9 - length(utr))
        println(cur, sp(3), "|", sp(3), utr, ssu, "|", sp(3), "\$", ss1, a, sp(3), "|", ss2,  p,"%")
    end
    br(28)
    println("|  Total value is: \$", tvs, "  |")
    br(28)
end



function percentage_exchange(ab::Dict{Symbol, Dict{Symbol,Float64}}, f::Feather, cg::ConstGraph, l::ReentrantLock, denom_order)
    c_cur = denom_order[1]
    cc = map(dic -> dic |> keys |> collect, values(ab)) |> y->foldl(vcat,y) |> union |> sort
end

function totals_to_percs(d::Dict{Symbol, Float64})
    tv = sum(values(d))
    out = Dict{Symbol, Float64}()
    for (k,v) in d
        out[k] = v / tv
    end
    out
end

function show_exchange_surplus(pt::Dict, target_percentage::Float64)
    target_value = pt[:total] / length(pt[:total_exchange]) * target_percentage
    out = Dict{Symbol, Float64}()
    for (k,v) in pt[:total_exchange]
        excess = target_value - v
        out[k] = excess
    end
    out
end

function show_exchange_transfers(pt::Dict, target_percentage::Float64=.95, threshold::Real=50)
    dinit = show_exchange_surplus(pt, target_percentage)
    d = copy(dinit)
    da = collect(d)
    from_list = sort(filter(x->x.second < 0, da), by=x->abs(x.second), rev=true)
    to_list = sort(filter(x->x.second >= 0, da), by=x->x.second, rev=true)

    out = Vector{Tuple{Pair{Symbol,Symbol}, Float64}}()
    for (fename, _) in from_list
        for (tename, _) in to_list
            famt = d[fename]
            tamt = d[tename]
            amt = min(abs(famt), tamt)
            if amt > threshold
                push!(out, (fename=>tename, amt))
                d[fename] += amt
                d[tename] -= amt
            end
        end
    end
    @assert sum(values(dinit)) == sum(values(d))
    out, d
end


function exchange_rebalance_amounts(pt::Dict,ename::Symbol, tgt_values::Dict{Symbol,<:Real})
    diff_raw = Dict{Symbol, Float64}()
    for (k,dic) in pt[:full]
        target_value = get(tgt_values,k, 0.0)
        amt_usd = dic[ename][1]
        dr = target_value - amt_usd
        pct = dr / amt_usd
        diff_raw[k] = dr
    end
    diff_raw
end

function buy_sell_list(dr::Dict{Symbol,Float64}, preferred::Vector{Symbol} = [:BTC, :ETH, :XRP], not_preferred::Vector{Symbol}=[:ETC])
    sell_list = Vector{Symbol}()
    buy_list = Vector{Symbol}()

    raw_sell =raw_sell = Vector{Symbol}()
    raw_buy = Vector{Symbol}()

    for (cur, qty) in dr
        if qty < 0
            push!(raw_sell, cur)
        else
            push!(raw_buy, cur)
        end
    end

    for x in not_preferred
        if x in raw_sell
            push!(sell_list, x)
        end
    end
    sell_list = vcat(sell_list, raw_sell) |> unique

    for x in preferred
        if x in raw_buy
            push!(buy_list, x)
        end
    end
    buy_list = vcat(buy_list, raw_buy) |> unique
    buy_list, sell_list
end
"""
function balance_trades(pt::Dict, ename::Symbol, target_value::Real, cg::ConstGraph, lcur::Symbol)
    # 2 step
    out1, dr = shift_surplus(pt, ename, target_value, cg, lcur)
    out2, dr2 = balance_trades(dr, ename, cg)
    vcat(out1,out2), dr2
end
"""

function check_pairs(bl::Vector{Symbol}, sl::Vector{Symbol}, ename::Symbol, cg::ConstGraph)
    tp = Vector{Tuple{Symbol,Symbol}}()
    for b in bl
        for s in sl
            push!(tp, (b,s))
            push!(tp, (s,b))
        end
    end
    filter!(x-> x in cg.pairs_dict[ename], tp)
end

function balance_trades(pt::Dict, ename::Symbol, tgt_values::Dict{Symbol,<:Real}, cg::ConstGraph, threshold::Real = 10.0, consolidate::Bool=true; aliases::Dict{Symbol,Symbol}=Dict{Symbol,Symbol}())
    drc = exchange_rebalance_amounts(pt, ename, tgt_values)
    ename_actual = Base.get(aliases, ename, ename)
    kk = keys(drc)
    for k in kk
        if !(ename_actual in cg.cur_exch[k])
            delete!(drc, k)
        end
    end
    balance_trades(drc, ename_actual, cg, threshold, consolidate)
end

function balance_trades(drc::Dict{Symbol, Float64}, ename::Symbol, cg::ConstGraph, threshold::Real, consolidate::Bool=true)
    preferred = [:BTC, :ETH, :USD, :XRP]
    #filter!(x-> x in keys(dr), preferred)
    not_preferred = [:ETC]
    #filter!(x-> x in keys(dr), not_preferred)

    bl, sl = buy_sell_list(drc, preferred, not_preferred)
    out = Vector{Tuple{Symbol, Tuple{Symbol,Symbol}, Float64}}()
    #    out = Vector{Tuple{Pair{Symbol,Tuple{Symbol,Symbol}},Float64}}()

    #@assert length(bl) > 0
    #@assert length(sl) > 0

    for s in sl
        for b in bl
            amt = min(abs(drc[s]), drc[b])

            gng_bool, sig = buy_or_sell(b,s,ename, cg)

            if amt > 0
                if !gng_bool
                    ging_iter = true
                    for b_i in preferred
                        bt1, sig1 = buy_or_sell(b_i, s, ename, cg)
                        bt2, sig2 = buy_or_sell(b, b_i, ename, cg)
                        if (bt1 & bt2) && ging_iter
                            push!(out,  (sig1.first, sig1.second, amt))
                            push!(out,  (sig2.first, sig2.second, amt))
                            drc[s] += amt
                            drc[b] -= amt
                            ging_iter = false
                        else
                        end
                    end
                else
                    drc[s] += amt
                    drc[b] -= amt
                    push!(out, (sig.first, sig.second, amt))
                end
            end
        end
    end
    if consolidate
        out = condense_trades(out)
    end
    out_filt = typeof(out)()
    map(out) do x
        amt = x[3]
        if amt < threshold   # undo from drc
            (b, s) = x[1] == :buy ? x[2] : reverse(x[2])
            drc[s] -= amt
            drc[b] += amt
        else
            push!(out_filt, x)
        end
    end
    drcout = typeof(drc)()
    for (k,v) in drc
        drcout[k] = round(v, digits=2)
    end
    out_filt, drcout
end

function condense_trades(arr::Array{Tuple{Symbol,Tuple{Symbol,Symbol},Float64},1})
    dict = Dict{Tuple{Symbol,Symbol}, Float64}()
    for r in arr
        pair = r[2]
        amt = r[3]
        cv = get(dict, pair, 0.0)
        mult = r[1] == :buy ? 1 : -1
        dict[pair] = cv + mult * amt
    end
    pp = [x[2] for x in arr] |> unique
    map(pp) do pair
        amt = dict[pair]
        amt < 0 ? (:sell, pair, -1*amt) : (:buy, pair, amt)
    end
end

function build_rebalance_orders(vect::Vector{Tuple{Symbol, Tuple{Symbol,Symbol},Float64}}, ename::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock, h::UInt64, limit_order=[])
    map(vect) do (side, pair, dollar_amt)
        dollar_price = lookup_price((pair[1],:USD), side, ename, f, cg, l) # prices[(pair[1], :USD)]
        price, qs = lookup_price(pair, side, ename, f, cg, l, true)
        amt = round(dollar_amt / dollar_price, digits=8)
        ttype = pair in limit_order ? LimitOrder : MarketOrder
        ts = time()
        order = ttype(
            ename,
            pair,
            side,
            amt,
            price,
            ts-qs,
            ts,
            999999,
            0.0,
            h
        )
    end
end

function shift_surplus(pt::Dict, ename::Symbol, tgt_values::Dict{Symbol,Real}, cg::ConstGraph, surplus_cur::Symbol, threshold::Real, consolidate::Bool=true; aliases::Dict{Symbol,Symbol}=Dict{Symbol,Symbol}())
    drc = exchange_rebalance_amounts(pt, ename, tgt_values)
    ename_actual = Base.get(aliases, ename, ename)
    shift_surplus(drc, ename_actual, cg, surplus_cur, threshold, consolidate)
end

function shift_surplus(drc::Dict{Symbol, Float64}, ename::Symbol, cg::ConstGraph, surplus_cur::Symbol, threshold::Real, consolidate::Bool=true)
    preferred = [:BTC, :ETH, :USD, :XRP]

    bl, sl = buy_sell_single(drc, surplus_cur, preferred)
    @assert length(bl) == 1
    b = surplus_cur
    #out = Vector{Tuple{Pair{Symbol,Tuple{Symbol,Symbol}},Float64}}()
    out = Vector{Tuple{Symbol, Tuple{Symbol,Symbol}, Float64}}()

    for s in sl
        amt = abs(drc[s])
        if amt > threshold
            gng_bool, sig = buy_or_sell(b,s,ename, cg)

            if !gng_bool
                ging_iter = true
                for b_i in preferred
                    bt1, sig1 = buy_or_sell(b_i, s, ename, cg)
                    bt2, sig2 = buy_or_sell(b, b_i, ename, cg)
                    if (bt1 & bt2) && ging_iter
                        push!(out,  (sig1.first, sig1.second,  amt))
                        push!(out,  (sig2.first, sig2.second,  amt))
                        drc[s] += amt
                        drc[b] -= amt
                        ging_iter = false
                    end
                end
            else
                drc[s] += amt
                drc[b] -= amt
                push!(out, (sig.first, sig.second, amt))
            end
        end
    end
    if consolidate
        out = condense_trades(out)
    end
    out_filt = typeof(out)()
    map(out) do x
        amt = x[3]
        if amt < threshold   # undo from drc
            (s,b) = x[2]
            drc[s] -= amt
            drc[b] += amt
        else
            push!(out_filt, x)
        end
    end
    drcout = typeof(drc)()
    for (k,v) in drc
        drcout[k] = round(v, digits=2)
    end
    out_filt, drcout
end

function buy_sell_single(dr::Dict{Symbol,Float64},new_cur::Symbol=:XRP, preferred::Vector{Symbol}=Vector{Symbol}())
    buy_list = [new_cur]
    sell_list = Vector{Symbol}()

    for (cur, qty) in dr
        if qty < 0
            push!(sell_list, cur)
        end
    end

    # sell preferrrred currencies last
    fp = filter(x-> x in sell_list, preferred)
    sell_list = vcat(fp, sell_list) |> unique |> reverse

    buy_list, sell_list
end


function buy_or_sell(cur_add::Symbol, cur_subtract::Symbol, ename::Symbol, cg::ConstGraph)
    buy_or_sell(cur_add, cur_subtract, cg.pairs_dict[ename])
end

function buy_or_sell(cur_add::Symbol, cur_subtract::Symbol, arr::Array{Tuple{Symbol,Symbol},1})
    if (cur_add, cur_subtract) in arr
        return true, :buy=>(cur_add, cur_subtract)
    elseif (cur_subtract, cur_add) in arr
        return true, :sell => (cur_subtract, cur_add)
    else
        return false, :no_pair => (cur_add, cur_subtract)
    end
end

function order_exchange_transfers(pt::Dict)
    etn = show_exchange_transfers(pt)
    receive_list = Vector{Pair{Symbol, Float64}}()
    send_list = Vector{Pair{Symbol, Float64}}()

    for p in collect(etn)
        if p.second <= 0
            push!(send_list, p)
        else
            push!(receive_list, p)
        end
    end

    se_list = [x.first for x in sort(send_list, by=x-> x.second)]
    re_list = [x.first for x in sort(receive_list, by=x-> x.second)]

    out = []
    for se in se_list
        for re in re_list
            transfer_amt = min(-1 * etn[se], etn[re])
            if transfer_amt > 0
                r = (se=> -1 * transfer_amt, re => transfer_amt)
                push!(out, r)
                etn[se] += transfer_amt
                etn[re] -= transfer_amt
            end
        end
    end
    out, etn
end

function format_address(ename::Symbol, cur::Symbol, d::Dict)
    dd = d[string(ename)]
    if cur in [:XRP]
        addr = dd[string(cur)]
        tag = dd[string(cur, "_tag")]
        return TagAddress(addr,tag)
    else
        addr = dd[string(cur)]
        return StandardAddress(addr)
    end
end

function build_with_precision( pt::Dict,f::Feather, cg::ConstGraph, l::ReentrantLock, tgt_values::Dict{Symbol,Real}, threshold::Real, h::UInt64)
    out = Vector{Order}()
    for ename in keys(cg.pairs_dict)
        oo = build_with_precision(ename, pt, f, cg, l, tgt_values, threshold, h)
        out = vcat(out, oo)
    end
    out
end

function build_with_precision(ename::Symbol,  pt::Dict,f::Feather, cg::ConstGraph, l::ReentrantLock, tgt_values::Dict{Symbol,Real}, threshold::Real, h::UInt64)
    # build orders using most up-to-date prices
    rb_orders = graph_rebalance_orders(ename, pt, f, cg, l, tgt_values, threshold, h)
    build_with_precision(rb_orders)
end

function build_with_precision(rb_orders::Vector{<:Order})
    enames = [x.feed for x in rb_orders] |> union
    #@assert length(enames) == 1
    ename = enames[1]
    out = if ename in keys(ClientSizeDict)
        map(x-> precision_check(x, ClientSizeDict[ename]), rb_orders)
    else
        rb_orders
    end
    filter!(out) do order
        order.price != Inf
    end
    out
end


function transfer(amt::Float64, from::Symbol, to::T, currency::Symbol) where { T <: CryptoAddress }
    client = ClientDict[from]
    ak = api_keys[from]
    transfer(amt, client, ak, to, currency)
end


function transfer(amt::Float64, client::NonceClient, ak::APIKey, to::T, currency::Symbol) where { T <: CryptoAddress }
    last_nonce = client.nonce
    client.nonce = max(last_nonce + 1, time())
    client.transfer(currency, amt, to, ak, client.nonce)
end

function transfer(amt::Float64, client::Client, ak::APIKey, to::T, currency::Symbol) where { T <: CryptoAddress }
    client.transfer(currency, amt, to, ak)
end


function xrp_transfer(otp::Tuple{Pair{Symbol,Symbol},Float64})
    xrp_transfer(otp[2], otp[1].first, otp[1].second)
end

function xrp_transfer(amt_dollar::Real, from::Symbol, to::Symbol)
    amt_xrp = currency_convert(amt_dollar, :USD, :XRP, from, f, cg, l)
    @assert pt[:full][:XRP][from][3] >= amt_xrp

    to_address = xrp_addresses[to]
    @assert typeof(to_address) == TagAddress
    transfer(amt_xrp, from, to_address, :XRP)
end


function build_rebalance_orders(pt::Dict, f::Feather, cg::ConstGraph, l::ReentrantLock, tgt_values::Dict{Symbol,Real}, threshold::Real, h::UInt64)
    enames = pt[:total_exchange] |> keys |> collect
    out = map(enames) do ename
        build_rebalance_orders(pt, ename, tgt_values, threshold, f, cg, l, h)
    end |> y->foldl(vcat, y)
    convert(Vector{Order}, out)
end

function build_rebalance_orders(pt::Dict, ename::Symbol, tgt_values::Dict{Symbol,Real}, threshold::Real, surplus_cur::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock, h::UInt64)

    out_balance, dr_balance = balance_trades(pt, ename, tgt_values, cg, threshold)
    out_surplus, dr_both = shift_surplus(dr_balance, ename, cg, surplus_cur, threshold)

    out = vcat(out_balance, out_surplus)
    limit_dict = Dict(:coinbasepro => [(:EOS, :BTC)])
    ll = get(limit_dict, ename, [])

    precise_orders = build_with_precision(out, ename, f, cg, l, h, ll)
    filter!(precise_orders) do order
        order.price != Inf
    end
end
