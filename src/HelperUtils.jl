import Base.filter, Base.filter!

function unit_round(amt::Float64, symb::Symbol)
    nd = Dict(
        :BTC => 4,
        :ETC => 2,
        :BCH => 4,
        :USD => 2,
        :EOS => 2,
        :XRP => 2,
        :LTC => 4,
        :ETH => 4
    )
    #spd = Dict(2 => "%.2f", 4=> "%.4f")
    n = get(nd, symb, 4)
    ar = round(amt, digits=n)
end

function precision_calc(raw_n::Real, n_digits::Real)
    #if raw_n < 1
        floor(raw_n, digits = n_digits)
    #else
     #   ii = 1 + (log10(raw_n) |> floor |> Int)
      #  round(raw_n, digits = n_digits - ii)
    #end
end

function stop!(t::Task)
    Base.throwto(t, InterruptException())
end

function clear!(c::Channel)
    ll = length(c.data)
    if ll > 0
        for t in length(c.data)
            take!(c)
        end
    end
end

function clear!(v::Vector)
    l = length(v)
    if l > 0
        for i=1:l
            pop!(v)
        end
    end
    v
end

function email(mail_rcpt::String, subject::String, body::String)
    @assert length(split(mail_rcpt, "@")) == 2
    @assert length(split(mail_rcpt, ".")) > 1
    run(pipeline(`echo $body`, `mail -s $subject $mail_rcpt`))
end


function filter(func::Function, dict::Dict)
    od = typeof(dict)()
    for (k,v) in dict
        if func(v)
            od[k] = v
        end
    end
    od
end

function filter!(func::Function, dict::Dict)
    for (k,v) in dict
        if !func(v)
            delete!(dict, k)
        end
    end
    dict
end


function parse_response(t::Task)
    t |> fetch |> parse_response
end

function parse_response(r::HTTP.Messages.Response)
    r.body |> String |> JSON.parse
end

function reset_toggle_channel(togg::Channel)
    for i=1:length(togg.data)
        take!(togg)
    end
    put!(togg, true)
    put!(togg, false)
    true
end


symdic(x) = Dict(Symbol(k)=>v for (k,v) in x)
symmap(x) = map(Symbol, x)

function safe_parse_F64(x::String)
    parse(Float64, x)
end

function safe_parse_F64(x::Int)
    convert(Float64, x)
end

function safe_parse_F64(x::Float64)
    x
end

function safe_parse_F64(x::Any)
    try
        parse(Float64,x)
    catch
        x
    end
end

function def_parse_F64(x::Any, def::Float64)
    try
        parse(Float64, x)
    catch
        def
    end
end

#=
function setup_sockets(config_socket::Socket, query_list::Dict, ctx)
    zmq_network = get_config_x(config_socket, "zmq_network")
    socket_dic = Dict{Symbol, Socket}()

    for (socket_name, socket_type) in query_list
        port = zmq_network[string(socket_name)]
        socket_dic[socket_name] = setup_socket(socket_type,port, ctx)
    end
    socket_dic
end

function c_control(in_channel::Channel, rep::Channel, out_channel::Channel, func, args...)
    tog = Channel{Bool}(2)
    for msg in in_channel
        if msg == :start
            put!(rep, "starting")
            for i=1:length(tog.data)
                take!(tog)
            end
            put!(tog,true)
            put!(tog, false)
            @async begin
                while fetch(tog)
                    out = func(args...)
                    put!(out_channel, out)
                end
            end
        elseif msg == :stop
            put!(rep, "stopping")
            length(tog.data) > 0 ? take!(tog) : false
        elseif msg == :status
            status = length(tog.data) > 0 ? fetch(tog) : false
            out = status ?  "running" : "stopped"
            put!(rep, out)
        else
            put!(rep, "bad message; continuing")
        end
    end
    println("done!")
end

# version with no out channel
function c_control(in_channel::Channel, rep::Channel, func, args...)
    tog = Channel{Bool}(2)
    for msg in in_channel
        if msg == :start
            put!(rep, "starting")
            for i=1:length(tog.data)
                take!(tog)
            end
            put!(tog,true)
            put!(tog, false)
            @async begin
                while fetch(tog)
                    out = func(args...)
                end
            end
        elseif msg == :stop
            put!(rep, "stopping")
            length(tog.data) > 0 ? take!(tog) : false
        elseif msg == :status
            status = length(tog.data) > 0 ? fetch(tog) : false
            out = status ?  "running" : "stopped"
            put!(rep, out)
        else
            put!(rep, "bad message; continuing")
        end
    end
    println("done!")
end

function z_control(csock::Socket, func, args...)
    a = Channel(10)
    b = Channel(10)
    @async c_control(a, b, func, args...)
    keep_going = true
    println("init")
    while keep_going
        msg = recv(csock) |> String
        println(msg)
        if msg in ["start", "stop", "status"]
            put!(a, Symbol(msg))
            rep_msg = take!(b)
        elseif msg == "kill"
            put!(a, :stop)
            rep = take!(b)
            keep_going = false
            rep_msg = string("killing; status is:", rep)
        else
            rep_msg = "bad message; continuing"
        end
        println(rep_msg)
        send(csock, rep_msg)
    end
    println("goodbye")
end
=#
