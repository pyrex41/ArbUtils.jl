module Bitstamp

export client

using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, Nettle, UUIDs, Printf

function req_private(method::String, path::String, ak::APIKey)
    req_private(method, path, Dict(), ak)
end

function req_private(method::String, path::String, query::Dict, ak::APIKey)
    method = uppercase(method)
    @assert method in ["GET", "POST", "DELETE"]

    base_url = "www.bitstamp.net"

    path = string("/api/v2/", lstrip(path, '/'))
    t = time() * 1000
    timestamp = @sprintf "%.0f" t

    nonce = uuid4() |> string

    api_key = ak.api_key
    hmac_key = ak.secret |> String
    uid = ak.uid

    uri = HTTP.URI(scheme="https", host=base_url, path=path, query=query)
    headers = Array{Pair{SubString{String},SubString{String}},1}()
    content_string = ""
    body = string(uri.query)

    if method == "POST"
        query = length(query) > 0 ? query : Dict("foo"=>"bar")
        uri_post = HTTP.URI(scheme="https", host=base_url, path=path, query=query)
        body = uri_post.query
        content_string = "application/x-www-form-urlencoded"
        push!(headers, "Content-Type" => content_string)
        uri = HTTP.URI(scheme="https", host=base_url, path=path)
    end


    xauth_version = "v2"
    api_string = string("BITSTAMP ", api_key)
    query_string = length(uri.query) > 0 ? string("?",uri.query) : ""

    message = string(
        api_string,
        method,
        uri.host,
        uri.path,
        query_string,
        "/",
        content_string,
        nonce,
        timestamp,
        xauth_version,
        body
        )

    signature = hexdigest("sha256", hmac_key, message |> String)


    push!(headers, "X-Auth" => api_string)
    push!(headers, "X-Auth-Signature" => signature)
    push!(headers, "X-Auth-Nonce" => nonce)
    push!(headers, "X-Auth-Timestamp" => timestamp)
    push!(headers, "X-Auth-Version" => xauth_version)


    uri = HTTP.URI(scheme="https", host=base_url, path=path)
    if length(body) > 0
        @assert method != "GET"
        HTTP.request(method, uri, headers=headers, body=body)
    else
        HTTP.request(method, uri, headers=headers)
    end
end

get(args...) = req_private("GET", args...)

post(args...) = req_private("POST", args...)

function place_order(order::LimitOrder, ak::APIKey)
    @assert order.feed == :bitstamp
    sym_string = string(order.pair[1], order.pair[2]) |> lowercase
    path = string(order.side, "/", sym_string)
    query = Dict(
        :amount => order.amount,
        :price => order.price
    )
    req_private("POST", path, query, ak)
end


function place_order(order::MarketOrder, ak::APIKey)
    @assert order.feed == :bitstamp
    sym_string = string(order.pair[1], order.pair[2]) |> lowercase
    path = string(order.side, "/market/", sym_string)
    query = Dict(
        :amount => order.amount
    )
    return query
    req_private("POST", path, query, ak)
end

function cancel_order(id::String, ak::APIKey)
    query = Dict(:id => id)
    req_private("POST", "cancel_order", query, ak)
end

function fetch_order(id::String, ak::APIKey)
    query = Dict(:id => id)
    req_private("POST", "order_status", query, ak)
end

function fetch_order(pair::Tuple{Symbol,Symbol}, id::String, ak::APIKey)
    fetch_order(id,ak)
end

function fetch_balances(ak::APIKey)
    req_private("POST", "balance", ak) |> parse_response |> parse_balances
end


function transfer(currency::Symbol, amt::Float64, addr::TagAddress, ak::APIKey)
    s = string(lowercase(string(currency)),"_withdrawal")
    query = Dict(
        "amount" => amt,
        "address" => addr.address,
        "destination_tag" => addr.tag
    )
    req_private("POST", s, query, ak)
end

function transfer(currency::Symbol, amt::Float64, addr::StandardAddress, ak::APIKey)
    s = string(lowercase(string(currency)),"_withdrawal")
    query = Dict(
        "amount" => amt,
        "address" => addr.address,
    )
    req_private("POST", s, query, ak)
end

function parse_balances(d::Dict)
    out = Array{Pair{Symbol, Float64},1}()
    for (k,v) in d
        s = r"_available"
        if occursin(s, k)
            n = split(k,"_")[1] |> uppercase |> Symbol
            vv = safe_parse_F64(v)
            if vv > 0
                push!(out, n => vv)
            end
        end
    end
    sort!(out, by=x->x.first)
end
#=
function parse_order_response(d::Dict)
    try
        out = Dict{Symbol, Any}()
        out[:price_executed] = d["price"]
        out[:status] = :UNKNOWN
        out[:exchange_id] = d["id"]
        return out
    catch
        pprintln(d)
        throw(ErrorException(d))
    end
end
=#

function parse_order_response(d::Dict)
    try
        out = Dict{Symbol, Any}()
        if d["status"] == "Finished"
            out[:status] = :FILLED
            td = d["transactions"][1]
            if td["type"] == 2 # market order
                out[:price_executed] = parse(Float64, td["price"])
            end
            out[:fee] = parse(Float64,d["fee"])
            out[:fee_cur] = :quote
        else
            out[:status] = d["status"] |> Symbol
        end
        out[:exchange_id] = d["id"]
        return out
    catch
        pprintln(d)
        throw(ErrorException(d))
    end
end

parse_eid(d::Dict) = Base.get(d, "id", "None")

client = make_client(place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_order_response, parse_eid)

end # end module
