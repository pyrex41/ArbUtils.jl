module CoinbasePro

export client

using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, Nettle, Printf, JSON

function place_order(order::Order, ak::APIKey)
    #@assert order.feed == :coinbasepro
    order_type = typeof(order) == LimitOrder ? :limit : :market
    query = Dict(
        :size => order.amount,
        :side => order.side,
        :product_id => string(order.pair[1], "-", order.pair[2])
    )
    if typeof(order) == LimitOrder
        query[:type] = :limit
        query[:price] = order.price
    else
        query[:type] = :market
    end

    req_private("POST", "/orders", query, ak)
end

function req_private(method::String, path::String, query::Dict, apikeys::APIKey)
    base_url = "api.pro.coinbase.com"

    path = path[1] == '/' ? path : "/"*path
    t = time()
    timestamp = @sprintf "%.0f" t

    uri = HTTP.URI(scheme="https", host=base_url, path=path)

    message = string(timestamp, method, path)
    if !isempty(query)
        message = message * json(query)
    end
    hmac_key = Base64.base64decode(apikeys.secret)
    sb64 = digest("sha256", hmac_key, message |> String) |> Base64.base64encode
    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "CB-ACCESS-SIGN"=> sb64)
    push!(headers, "CB-ACCESS-TIMESTAMP"=> timestamp)
    push!(headers, "CB-ACCESS-KEY"=> apikeys.api_key)
    push!(headers, "CB-ACCESS-PASSPHRASE"=> apikeys.password)
    push!(headers, "Content-Type"=> "application/json")
    HTTP.request(method, uri, headers=headers, body=json(query))
end



function req_private(method::String, path::String, apikeys::APIKey)
    base_url = "api.pro.coinbase.com"

    path = path[1] == '/' ? path : "/"*path
    t = time()
    timestamp = @sprintf "%.0f" t

    uri = HTTP.URI(scheme="https", host=base_url, path=path)

    message = string(timestamp, method, path)
    hmac_key = Base64.base64decode(apikeys.secret)
    sb64 = digest("sha256", hmac_key, message |> String) |> Base64.base64encode
    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "CB-ACCESS-SIGN"=> sb64)
    push!(headers, "CB-ACCESS-TIMESTAMP"=> timestamp)
    push!(headers, "CB-ACCESS-KEY"=> apikeys.api_key)
    push!(headers, "CB-ACCESS-PASSPHRASE"=> apikeys.password)
    push!(headers, "Content-Type"=> "application/json")
    HTTP.request(method, uri, headers=headers)
end

get(args...) = req_private("GET", args...)

post(args...) = req_private("POST", args...)

function get(path::String)
    base_url = "api.pro.coinbase.com"
    path = path[1] == '/' ? path : "/"*path
    uri = HTTP.URI(scheme="https", host = base_url, path=path)
    HTTP.request("GET", uri)
end

function cancel_order(id::String, pair::Tuple{Symbol,Symbol}, ak::APIKey)
    query = Dict(
        "product_id" => string(pair[1], "-", pair[2])
    )
    req_private("DELETE", string("/orders/", id), query, ak)
end

function cancel_order(id::String, pair::String, ak::APIKey)
    query = Dict(
        "product_id" => pair
    )
    req_private("DELETE", string("/orders/", id), query, ak)
end

function fetch_order(id::String, ak::APIKey)
    req_private("GET", string("/orders/", id), ak)
end

function fetch_order(pair::Tuple{Symbol,Symbol}, id::String, ak::APIKey)
    fetch_order(id,ak)
end

function fetch_balances(ak::APIKey, all=false)
    r = get("accounts", ak)
    b = parse_response(r)
    bb = filter(b) do dic
        bal = safe_parse_F64(dic["balance"])
        all ? true : bal > 0
    end
    out = Array{Pair{Symbol, Float64},1}()
    map(bb) do dic
        p =Symbol(dic["currency"]) => safe_parse_F64(dic["balance"])
        push!(out, p)
    end
    sort!(out, by=x->x.first)
end

function transfer(currency::Symbol, amt::Float64, addr::TagAddress, ak::APIKey)
    query = Dict(
        "amount" => amt,
        "currency" => currency |> string,
        "crypto_address" => addr.address,
        "destination_tag" => addr.tag
    )
    req_private("POST", string("/withdrawals/crypto"), query, ak)
end

function transfer(currency::Symbol, amt::Float64, addr::StandardAddress, ak::APIKey)
    query = Dict(
        "amount" => amt,
        "currency" => currency |> string,
        "crypto_address" => addr.address
    )
    req_private("POST", string("/withdrawals/crypto"), query, ak)
end

function parse_order_response(d::Dict)
    out = Dict{Symbol, Any}()
    s = d["status"]
    if s=="done" && d["done_reason"] == "filled"
        out[:status] = :FILLED
        if d["type"] == "market"
            order_total = parse(Float64, d["executed_value"])
            amt = parse(Float64, d["filled_size"])
            out[:price_executed] = order_total / amt
        else
            out[:price_executed] = parse(Float64,d["price"])
        end
        out[:fee] = parse(Float64, d["fill_fees"])
        out[:fee_cur] = :quote
    else
        out[:status] = Symbol(s)
    end
    out[:exchange_id] = get(d, "id", :None)
    out
end

parse_check_response(d::Dict) = parse_order_response(d)

parse_eid(d::Dict) = Base.get(d, "id", "None")

new_client() = make_client(place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_check_response, parse_eid)

client = new_client()

end # end module
