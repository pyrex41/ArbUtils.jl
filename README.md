# ArbUtils

Package to find and quickly update higher order (3+step) arbs across different currencies and exchanges

### Overview
* models all unique (exchange, currency) pairs as nodes on a directed graph
* connects each possible trading pair as a weighted edge between nodes
* (exchange A, currency X) <--> (exchange B, currency X) nodes are weighted with weight of 1
* Use graph analysis package to find all complete cyclces (eg arbs) in the graph. Filter out those that are too long to be of interest
* Store possible arb paths as ordered tuples of indices corresponding to sparse matrix holding price data
* Use websockets to asynchronously update prices in sparse matrix (utilizing locks when updating data)
* Calculate arbs by multiplying prices in to get arb return. If > 1.0 (net of fees), this is an arb.
