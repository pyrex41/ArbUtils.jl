module SFOX

export client

using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, JSON

function req_private(method::String, path::String, query::Dict, ak::APIKey)
    method = uppercase(method)
    @assert method in ("GET", "POST")

    base_url = "api.sfox.com"
    path = string("/v1/", lstrip(path, '/'))

    api_key = ak.api_key
    auth=Dict("Authorization" => "Basic " * Base64.base64encode(api_key))

    if method == "POST"
        uri = HTTP.URI(scheme="https", host=base_url, path=path)
        HTTP.post(uri, query=query, headers = auth)
    else
        uri = HTTP.URI(scheme="https", host=base_url, path=path, query=query)
        HTTP.request(method, uri, headers=auth)
    end
end

function req_private(method::String, path::String, ak::APIKey)
    req_private(method, path, Dict(), ak)
end

get(args...) = req_private("GET", args...)

post(args...) = req_private("POST", args...)

function place_order(order::Order, api_key::APIKey)
    @assert order.feed == :sfox

    str_pair = string(order.pair[1],order.pair[2]) |> lowercase
    path = string("orders/", order.side)

    query = Dict{Symbol, Any}(
        :currency_pair => str_pair,
        :client_order_id => order.hash,
        :quantity => order.amount,
        :routing_type => :NPR # Net price routing; see https://www.sfox.com/blog/sfox-pricing-crypto-trading-net-price-routing/
    )
    if typeof(order) == LimitOrder
        query[:routing_id] = 200 # smart order routing; see https://www.sfox.com/developers/#algorithms and https://www.sfox.com/algos/
        query[:price] = order.price
    else
        query[:routing_id] = 100
        query[:amount] = order.price * order.amount
    end
    post(path, query, ak) |> parse_response
end

function cancel_order(id::String, ak::APIKey)
    path = string("orders/", id)
    req_private("DELETE", path, ak) |> parse_response
end

function fetch_order(id::String, ak::APIKey)
    path = string("orders/", id)
    get(path, ak) |> parse_response
end

function fetch_balances(ak::APIKey)
    r = req_private("GET", "user/balance", ak) |> parse_response
    out = Dict{Symbol, Float64}()
    for dic in r
        k = dic["currency"] |> uppercase |> Symbol
        v = dic["balance"] |> safe_parse_F64
        out[k] = v
    end
    out
end

function transfer(currency::Symbol, amt::Float64, addr::CryptoAddress, ak::APIKey)
    query = Dict(
        "amount" => amt,
        "currency" => currency |> string,
        "address" => addr.address
    )
    req_private("POST", string("user/withdraw"), query, ak)
end

function parse_order_response(d::Dict)
    Dict{Symbol, Any}(
        :status => d["status"] |> Symbol,
        :exchange_id => d["id"]
    )
end

parse_check_response(d::Dict) = parse_order_response(d)

parse_eid(d::Dict) = Base.get(d, "id", "None")

client = make_client(place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_check_response, parse_eid)

end # end module

#=
function format_feed_sfox(pair::Tuple,feed_type="orderbook")
    base_str = feed_type* ".sfox."
    string(base_str, foldl(string, pair)) |> lowercase
end

struct XTick
    side::Symbol
    price::Float64
    size::Float64
    feed::String
    pair::Tuple{Symbol,Symbol}
    time_received::Float64
    timestamp::Float64
end

function sfox_currencies(api_key::String, format_symbol=true)
    base_url = "https://api.sfox.com/v1/markets/currency-pairs"
    auth=Dict("Authorization" => "Basic " * Base64.base64encode(api_key))

    req = HTTP.get(base_url; headers=auth)
    markets = req.body |> String |> JSON.parse

    sfox_pairs = []

    for v in values(markets)
       str = v["formatted_symbol"]
       spl_i = split(str, "/")
       spl = format_symbol ? map(Symbol, spl_i) : spl_i
       push!(sfox_pairs, (spl[1],spl[2]))
   end
   sfox_pairs
end

function get_sfox_client(api_keys)
    api_key = api_keys["apiKey"]
    po(x) = sfox_order(x, api_key)
    lm() = sfox_currencies(api_key)
    SFOXClient(
        po,
        lm
    )
end

function collect_feeds(currencies::Array, api_key::String)

    sfox_pairs = sfox_currencies(api_key, false)

    #=
    sfox_pairs = [
        ("BTC","USD"),
        ("BCH","USD"),
        ("BSV","BTC"),
        ("ETH","BTC"),
        ("ETC","BTC"),
        ("ETC","USD"),
        ("ETH","USD")]
    =#
    fsfox = filter(sfox_pairs) do (p1,p2)
        p1 in currencies && p2 in currencies
    end

    sub_dict = Dict(
        "type"=>"subscribe",
        "feeds"=> map(format_feed_sfox, fsfox))
    pdic = Dict()
    payloads = []
    WebSockets.open("wss://ws.sfox.com/ws") do ws_client
        writeguarded(ws_client, sub_dict |> JSON.json)
        fnow() = now() |> datetime2unix
        tn = fnow()

        while fnow() < tn + 5
            data, success = readguarded(ws_client)
            if success
                body = String(data) |> JSON.parse
                println(stderr, ws_client, " received:")
                if haskey(body, "type")
                    println(body)
                else
                    try
                        payload = body["payload"]
                        push!(payloads, payload)
                        println("**PAYLOAD**")

                        exchanges = payload["timestamps"] |> keys |> collect
                        feeds = map(x-> "sfox:"*x, exchanges)

                        farray = get(pdic, payload["pair"], [])
                        pprintln(farray)
                        pdic[payload["pair"]] = union(farray, feeds)

                    catch
                        println("error: sequence: ", body["sequence"])
                        println("recipient: ", body["recipient"])
                    end
                end
                println()
            end
        end
    end
    pdic
end


function process_payload(payload::Dict{String,Any}, quote_holder::Dict)
    time_received = now() |> datetime2unix
    ticks = process_payload(payload, time_received)
    process_ticks(ticks, quote_holder)
end

function process_payload(payload::Dict{String,Any}, time_received::Float64)
    mm = payload["market_making"]
    ask_rows = get(mm, "asks", Dict())
    bid_rows = get(mm, "bids", Dict())

    ps_format(ps) = (Symbol(ps[1:3]|> uppercase), Symbol(ps[4:end] |> uppercase))
    pair = ps_format(payload["pair"])

    function _xpub_parse(side::Symbol, rows::Array, pair::Tuple{Symbol,Symbol}, ts_rows::Dict, time_received)
        @assert side in [:bid, :ask]
        ii = side == :bid ? 1 : 2
        side_label = string(side)
        side_vol_label = side_label * "_volume"
        ticks = []
        for row in rows
            price, size, feed_r = row
            feed = "sfox:"* feed_r |> uppercase
            timestamp = ts_rows[feed_r][ii]
            if !isreal(timestamp)
                timestamp = parse(Float64, timestamp)
            end
            timestamp = convert(Float64, timestamp)
            xt = XTick(
                side,
                price,
                size,
                feed,
                pair,
                time_received,
                timestamp
            )
            push!(ticks, xt)
        end
        ticks
    end
    a = _xpub_parse(:ask, ask_rows, pair, payload["timestamps"], time_received)
    b = _xpub_parse(:bid, bid_rows, pair, payload["timestamps"], time_received)
    vcat(a,b)
end

skeleton_book(feed, pair) = Dict(
    :ask => 0.0,
    :ask_volume => 0.0,
    :bid => 0.0,
    :bid_volume => 0.0,
    :time_received => now() |> datetime2unix,
    :timestamp => 0.0,
    :feed => feed,
    :pair => pair
)

function xtick_quote_update(xtick::XTick, quote_holder::Dict)
    if !haskey(quote_holder, xtick.pair)
        quote_holder[xtick.pair] = Dict()
    end
    book = get(quote_holder[xtick.pair], xtick.feed, skeleton_book(xtick.feed, xtick.pair))

    vol_label = xtick.side == :bid ? :bid_volume : :ask_volume
    book[xtick.side] = xtick.price
    book[vol_label] = xtick.size
    for x in [:timestamp, :time_received]
        book[x] = getproperty(xtick, x)b
    end
    quote_holder[xtick.pair][xtick.feed] = book
end

function process_ticks(ticks::Array{Any, 1}, quote_holder::Dict)
    p_log = Dict()
    for tick in ticks
        flags = get(p_log, (tick.feed, tick.pair), [])
        push!(flags, tick.side)
        p_log[(tick.feed, tick.pair)] = flags
        xtick_quote_update(tick, quote_holder)
    end
    out = []
    for (ki, flags) in p_log
        feed, pair = ki
        book = quote_holder[pair][feed]
        od = copy(book)
        od[:flags] = flags
        od[:feed] = feed

        pair_str = string(pair[1],"-",pair[2])
        od[:pair] = pair_str
        push!(out, od)
    end
    out
end
=#
