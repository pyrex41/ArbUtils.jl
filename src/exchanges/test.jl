[push!(LOAD_PATH, pwd()*p) for p in ("", "/exchanges")]

using ZMQ, HelperUtils, SocketUtils, OrderUtils, ClientUtils, JSON

ts=:bittrex
using Bittrex

ctx = Context()
config_url = "tcp://127.0.0.1:5660"
config_query = Socket(ctx, REQ)
ZMQ.connect(config_query, config_url)

# inline utilities for easy config lookups
gc(x) = get_config_x(config_query,x)
qsocket(stype, nm) = setup_socket(stype, gc("zmq_network")[nm], ctx)
# amounts = symdic("amounts" |> gc) # this is not needed but can be used as a check

function parse_api_keys(rk)
    out = Dict{Symbol, APIKey}()
    for (exch,dic) in rk
        out[Symbol(exch)] = APIKey(
            dic["apiKey"],
            get(dic, "secret", ""),
            get(dic, "password", ""),
            get(dic, "uid", "")
        )
    end
    out
end

api_keys = gc("api_keys") |> parse_api_keys

ak = api_keys[ts]

o = LimitOrder(
    ts,
    (:ETH,  :BTC),
    :buy,
    .03,
    .03471,
    1.0,
    156,
    556,
    .99,
    hash(1)
)
