import Base.copy

function dynamic_graph(ag::ExpressiveGraph)
    DynamicGraph(
        ag.weight_matrix,
        ag.update_matrix,
        ag.bids,
        ag.asks,
        ag.spreads
    )
end

function snapshot(dg::ExpressiveGraph)
    SnapshotGraph(
        copy(dg.weight_matrix),
        copy(dg.update_matrix),
        copy(dg.bids),
        copy(dg.asks),
        copy(dg.spreads),
        datetime2unix(now())
    )
end

function snapshot(dg::ExpressiveGraph, ts::Float64)
    SnapshotGraph(
        copy(dg.weight_matrix),
        copy(dg.update_matrix),
        copy(dg.bids),
        copy(dg.asks),
        copy(dg.spreads),
        ts
    )
end

function copy(f::Feather)
    Feather(
        copy(f.weight_matrix),
        copy(f.bid_ask),
        copy(f.time_log),
        #copy(f.gonogo)
        #copy(f.spread_list)
    )
end

function feather(ag::ExpressiveGraph)
    w = copy(ag.weight_matrix)
    Feather(
        w,
        zeros(w),
        zeros(w),
        #SparseMatrixCSC(w.m, w.n, w.colptr, w.rowval, ones(Bool, length(w.nzval)))
        #zeros(Int8, length(ag.cycle_dict_all))
    )
end

"""
function clear_spreads(f::Feather)
    Feather(
        f.weight_matrix,
        f.bid_ask,
        f.time_log,
        zeros(Int8, length(f.spread_list))
    )
end
"""
us
