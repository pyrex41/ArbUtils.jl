include("exchanges/SFOX.jl")
include("exchanges/Bitstamp.jl")
include("exchanges/Bittrex.jl")
include("exchanges/CoinbasePro.jl")
include("exchanges/Kraken.jl")
include("exchanges/BinanceUS.jl")

using .SFOX, .Bitstamp, .Bittrex, .CoinbasePro, .Kraken, .BinanceUS

const ClientDict = Dict(
    :binanceus => BinanceUS.client,
    :bitstamp => Bitstamp.client,
    :coinbasepro => CoinbasePro.client,
    :kraken => Kraken.client,
    :bittrex => Bittrex.client,
    :sfox => SFOX.client
)

for (k,v) in ClientDict
    if typeof(v) == NonceClient
        v.nonce = time() * 10e3
    end
end

const ClientChannels = Dict(
    :binanceus => Channel{Order}(50),
    :bitstamp => Channel{Order}(50),
    :coinbasepro => Channel{Order}(50),
    :kraken => Channel{Order}(50),
    :bittrex => Channel{Order}(50),
    :sfox => Channel{Order}(50),
)


# to handle size / order formatting
# Coinbasepro
const ClientSizeDict = Dict{Symbol,Dict{Tuple{Symbol,Symbol}, Dict{String, Real}}}()

function get_cbp_size()
    cbp_products = CoinbasePro.get("products") |> x->x.body |> String |> JSON.parse
    dd = Dict{Tuple{Symbol,Symbol},Dict{String, Real}}()
    for d in cbp_products
        kstring = d["id"]
        kk = map(Symbol, split(kstring, "-"))
        k = (kk[1],kk[2])
        ms_raw = parse(Float64, d["base_min_size"])
        ms = Int(round(-1 * log10(round(ms_raw, sigdigits=1)), digits=0))
        mpi_raw = parse(Float64, d["quote_increment"])
        mpi = Int(round(-1 * log10(round(mpi_raw, sigdigits=1)), digits=0))
        dd[k] = Dict("price_precision"=>mpi, "amt_precision"=>ms)
    end
    dd
end
ClientSizeDict[:coinbasepro] = get_cbp_size()

# BinanceUS
function get_binanceus_size()
    businfo = BinanceUS.get("exchangeInfo")
    dd = Dict{Tuple{Symbol,Symbol}, Dict{String, Real}}()
    for d in businfo["symbols"]
        k = (Symbol(d["baseAsset"]), Symbol(d["quoteAsset"]))
        fdic = filter(d["filters"]) do dic
            dic["filterType"] == "LOT_SIZE"
        end |> first

        mq = fdic["minQty"]
        mq = typeof(mq) == Array{String,1} ? mq[1] : mq
        ss = fdic["stepSize"]
        ss = typeof(ss) == Array{String,1} ? ss[1] : ss
        dd[k] = Dict("price_precision" => d["quotePrecision"] |> Int,
                     "amt_precision"=>d["baseAssetPrecision"] |> Int,
                     "min_quantity" => parse(Float64, mq),
                     "step_size" => parse(Float64, ss)
                     )
    end
    dd
end
ClientSizeDict[:binanceus] = get_binanceus_size()


function fetch_balance_helper(client::Client, ak::APIKey)
    client.fetch_balances(ak)
end

function fetch_balance_helper(client::NonceClient, ak::APIKey)
    n = max(client.nonce + 1, time()*10e6)
    client.nonce = n
    client.fetch_balances(ak, n)
end

function getClientBalances(api::Dict{Symbol, APIKey}, clientdict::Dict{Symbol, AbstractClient}=ClientDict; aliases::Dict{Symbol, Symbol}=Dict{Symbol,Symbol}())
    cb = Dict{Symbol, Dict{Symbol, Float64}}()
    getClientBalances(cb, api, clientdict; aliases=aliases)
end

getClientBalances(cb::Dict{Symbol, Dict{Symbol,Float64}}, api::Dict{Symbol, APIKey}) = getClientBalances(cb, api, ClientDict)

function getClientBalances(cb::Dict{Symbol, Dict{Symbol,Float64}}, api::Dict{Symbol, APIKey}, clientdict::Dict{Symbol, AbstractClient}; aliases::Dict{Symbol,Symbol}=Dict{Symbol,Symbol}())
    for (k_nominal, ak) in api
        k = Base.get(aliases, k_nominal, k_nominal)
        if k in keys(clientdict)
            client = clientdict[k]
            try
                d = Dict{Symbol,Float64}()
                arr = fetch_balance_helper(client, ak)
                for (a,b) in arr
                    d[a] = b
                end
                cb[k_nominal] = d
            catch e
                println("error for ", k_nominal)
                @show e
                println("=====================")
            end
        end
    end
    cb
end

function process_task(ot::OrderTask)
    client = ClientDict[ot.feed]
    process_task_helper(ot, client)
end

# need to process order response and evaluate execution status --> log, or put in pending,
# need to processs pending orders

function gather_agents(exchanges::Array{Symbol,1}, api_keys::Dict{Symbol,APIKey}, balances::Dict{Symbol, Any}, fees::Dict{String, Float64})
    agents = Dict{Symbol, Agent}()
    # catch assertion errors first
    for exch in exchanges
        for dic in (ClientList, api_keys, balances, fees)
            @assert exch in keys(dic)
        end
    end

    # make agents
    for exch in exchanges
        ak = api_keys[exch]
        bal = balances[exch]
        fee = fees[string(exch)]
        agents[exch] = make_agent(exch, ak, bal, fee)
    end
    agents
end
