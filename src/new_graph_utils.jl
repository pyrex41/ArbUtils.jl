function new_graph(config_query::Socket; max_length=8)
    gc(x) = get_config_x(config_query,x)

    cur_list = map(Symbol, gc("currencies"))
    pairs_dict_raw = gc("pairs")
    blacklist = gc("blacklist")
    equivalents = [map(Symbol,x) for x in  gc("equivalents")]
    fees = Dict(Symbol(k)=>v for (k,v) in gc("fees"))
    amounts = Dict(Symbol(k)=>v for (k,v) in gc("amounts"))

    pairs_dict = Dict{Symbol, Array{Tuple{Symbol, Symbol},1}}()
    exchanges = Array{Symbol,1}()
    exch_cur = Dict{Symbol, Array{Symbol,1}}()
    cur_exch = Dict{Symbol, Array{Symbol,1}}()
    graph_index = Dict{Tuple{Symbol,Symbol}, Int64}()
    rev_index = Dict{Int64, Tuple{Symbol,Symbol}}()
    for (k,v) in pairs_dict_raw
        bl = get(blacklist, k , [])
        str_list = filter(x-> x ∉ bl, v)
        plist = [(x[1],x[2]) for x in map(x->map(Symbol, split(x,"-")), str_list)]
        cur_list = vcat([x[1] for x in plist], [x[2] for x in plist]) |> union
        exchange = k |> Symbol
        pairs_dict[exchange] = plist
        exch_cur[exchange] = cur_list
        push!(exchanges, exchange)

        for c in cur_list
            i = length(graph_index)
            graph_index[(exchange, c)] = i+1
            rev_index[i+1] = (exchange, c)
            arr = get(cur_exch, c, [])
            push!(arr, exchange)
            cur_exch[c] = arr
        end
    end
    all_pairs = pairs_dict |> values |> Iterators.flatten |> union
    currencies = cur_exch |> keys |> collect

    g = SimpleDiGraph()
    g_n = length(graph_index)
    add_vertices!(g, g_n)
    path_ind = Dict()

    #return pairs_dict

    type_dict = Dict{Tuple{Int64,Int64}, Tuple}()
    tdlookup(a,b) = type_dict[(a,b)]
    tdlookup(a) = tdlookup(a...)

    for (exchange, pairs_list) in pairs_dict
        for pair in pairs_list
            v1 = graph_index[(exchange, pair[1])]
            v2 = graph_index[(exchange, pair[2])]
            type_dict[v1,v2] = (:sell, exchange, pair)
            add_edge!(g, v1, v2)
            type_dict[v2,v1] = (:buy, exchange, pair)
            add_edge!(g, v2, v1)
        end
    end

    for (cur, exch_list) in cur_exch
        combs = collect(combinations(exch_list,2))

        for comb in combs
            v1 = graph_index[(comb[1], cur)]
            v2 = graph_index[(comb[2], cur)]
            exch = (comb[1], comb[2])
            type_dict[v1,v2] = (:transfer, cur, exch)
            add_edge!(g, v1, v2)
            type_dict[v2,v1] = (:transfer, cur, exch)
            add_edge!(g, v2, v1)
        end
    end

    # to add edges for equivalent currencies (eg, stablecoins)
    for eq_list in equivalents
        filter!(x->x in cur_list, eq_list)
        cur = eq_list[1]
        equal_pairs = collect(combinations(eq_list,2))
        all_pair_list = []
        for (a,b) in equal_pairs
            for exch in cur_exch[a]
                push!(all_pair_list, (exch, a))
            end
            for exch in cur_exch[b]
                push!(all_pair_list, (exch, b))
            end
        end
        combs = combinations(all_pair_list,2) |> collect
        for (a,b) in combs
            v1 = graph_index[a]
            v2 = graph_index[b]
            #path_ind[v1=>v2] = (:transfer, (a[2], b[2]), a[1], :equal)
            type_dict[v1,v2] = (:equal, a, b)
            add_edge!(g, v1, v2)
            #path_ind[v2=>v1] = (:transfer, (b[2], a[2]), b[1], :equal)
            type_dict[v2,v1] = (:equal, a, b)
            add_edge!(g, v2, v1)
        end
    end

    cycle_list = simplecycles_limited_length(g, max_length)
    filter!(x->length(x)>2,cycle_list)

    function step_sequence(cycle::Vector{Int64})
        flist = filter(collect(zip(cycle, circshift(cycle, -1)))) do pair
            tdlookup(pair)[1] in (:buy, :sell)
        end
        sort(flist, by=c->c[1])
    end

    pairs_lists = filter(x->length(x)>0, map(step_sequence, cycle_list) |> union)

    NGraph(
        pairs_lists,
        cur_exch,
        graph_index,
        rev_index,
        type_dict,
        fees,
        amounts,
        pairs_dict
    )
end

function feather(cg::FixedGraph)
    matrix_labels = [:weight_matrix, :bid_ask]
    g_n = length(cg.graph_index)
    for m in matrix_labels
        exp = :($m = (zeros(Float64, $g_n, $g_n)))
        eval(exp)
    end

    for (cur, exch_list) in cg.cur_exch
        combs = collect(combinations(exch_list,2))
        for comb in combs
            v1 = cg.graph_index[(comb[1], cur)]
            v2 = cg.graph_index[(comb[2], cur)]
            weight_matrix[v1,v2] = 1.0
            weight_matrix[v2,v1] = 1.0
        end
    end

    time_log = zeros(Float64, size(weight_matrix))
    Feather(
        weight_matrix,
        bid_ask,
        time_log
    )
end

function get_cycle_returns(cyc::Array{Tuple{Int64,Int64},1}, wm::SparseMatrixCSC{Float64,Int64})
    foldl(*, map(x->wm[x],cyc))
end

function lookup_constant(ag::ExpressiveGraph)
    rev_path_index = Dict{Tuple{Symbol, Tuple{Symbol,Symbol}}, Pair{Int64,Int64}}()
    li = Array{Tuple{Symbol,Symbol,Tuple{Symbol,Symbol}},1}()
    pa = Dict{Pair{Int64, Int64},Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}}()
    for (k,v) in ag.path_index
        if v[1] == :trade
            rev_path_index[(v[3],v[2])] = k
            row = (v[4], v[3], v[2])
            push!(li, row)
            pa[k] = row
        end
    end

    ddd = Dict{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}, Vector{Int64}}()
    arb_index = Dict{Int64,Array{Tuple{Symbol,Symbol, Tuple{Symbol,Symbol}}}}()
    for (k_arb, v_array) in ag.arbs_all
        for k in li
            if k in v_array
                d_i = get(ddd, k, Vector{Int64}())
                push!(d_i, k_arb)
                ddd[k] = d_i
                arb_index[k_arb] = v_array
            end
        end
    end
    cda = copy(ag.cycle_dict_all)
    agi = copy(ag.graph_index)
    fees = copy(ag.fees)
    amounts = copy(ag.amounts)

    ConstGraph(
        pa,
        rev_path_index,
        li,
        ddd,
        cda,
        agi,
        arb_index,
        fees,
        amounts
    )
end
