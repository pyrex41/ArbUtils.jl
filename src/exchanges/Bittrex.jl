module Bittrex

export client

using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, Nettle, SHA, Printf, JSON


function req_private(method::String, path::String, ak::APIKey)
    req_private(method, path, Dict(), ak)
end

function req_private(method::String, path::String, query::Dict, ak::APIKey)
    method = uppercase(method)
    @assert method in ["GET", "POST", "DELETE", "HEAD"]

    base_url = "api.bittrex.com"

    path = string("/v3/", lstrip(path, '/'))
    t = time() * 1000
    timestamp = @sprintf "%.0f" t

    uri = HTTP.URI(scheme="https", host=base_url, path=path)

    body = method == "POST" ? JSON.json(query) : ""
    content_hash = body |> sha512 |> bytes2hex

    message = string(
        timestamp,
        uri,
        method,
        content_hash
    )
    # not implementing subaccount id for now

    api_key = ak.api_key
    secret = ak.secret

    signature = hexdigest("sha512", secret, message |> String)

    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "Api-Key"=> api_key)
    push!(headers, "Api-Timestamp" => timestamp)
    push!(headers, "Api-Content-Hash" => content_hash)
    push!(headers, "Api-Signature" => signature)
    push!(headers, "Content-Type"=> "application/json")

    if method == "POST"
        push!(headers, "Content-Type"=> "application/json")

        HTTP.request(method, uri, headers=headers, body=body)
    else
        uri = HTTP.URI(scheme="https", host=base_url, path=path, query=query)
        HTTP.request(method, uri, headers=headers)
    end
end

get(args...) = req_private("GET", args...)

post(args...) = req_private("POST", args...)

function place_order(order::Order, ak::APIKey)
    @assert order.feed == :bittrex
    query = Dict(
        :marketSymbol => string(order.pair[1],"-", order.pair[2]),
        :direction => order.side |> string |> uppercase,
        :quantity => order.amount
    )
    if typeof(order) == LimitOrder
        query[:limit] = order.price
        query[:type] = "LIMIT"
        query[:timeInForce] = :GOOD_TIL_CANCELLED
    else
        query[:type] = "MARKET"
        query[:timeInForce] = :IMMEDIATE_OR_CANCEL
    end
    req_private("POST", "orders", query, ak)
end

function cancel_order(id::String, ak::APIKey)
    path = string("orders/", id)
    req_private("DELETE", path, ak)
end

function fetch_order(id::String, ak::APIKey)
    path = string("orders/", id)
    req_private("GET", path, ak)
end

function fetch_order(pair::Tuple{Symbol,Symbol}, id::String, ak::APIKey)
    fetch_order(id,ak)
end

function fetch_balances(ak::APIKey)
    req_private("GET", "balances", ak) |> parse_response |> parse_balances
end

function transfer(currency::Symbol, amt::Float64, addr::TagAddress, ak::APIKey)
    query = Dict(
        "quantity" => amt,
        "currencySymbol" => currency |> string,
        "cryptoAddress" => addr.address,
        "cryptoAddressTag" => addr.tag
    )
    req_private("POST", string("withdrawals"), query, ak)
end

function transfer(currency::Symbol, amt::Float64, addr::StandardAddress, ak::APIKey)
    query = Dict(
        "quantity" => amt,
        "currencySymbol" => currency |> string,
        "cryptoAddress" => addr.address
        )
    req_private("POST", string("withdrawals"), query, ak)
end

function parse_balances(a::Array)
    out = Array{Pair{Symbol, Float64},1}()
    for d in a
        n = d["currencySymbol"] |> Symbol
        v = d["total"] |> safe_parse_F64
        if v > 0
            push!(out, n => v)
        end
    end
    sort!(out, by=x->x.first)
end

function parse_order_response(d::Dict)
    out = Dict{Symbol, Any}()
    if d["status"] == "CLOSED"
        out[:status] = :FILLED
        if d["type"] == "MARKET"
            order_total = parse(Float64,d["proceeds"])
            amt = parse(Float64,d["fillQuantity"])
            out[:price_executed] = order_total / amt
        end
        out[:fee] = parse(Float64, d["commission"])
        out[:fee_cur] = :quote
    else
        out[:status] = d["status"] |> Symbol
    end
    out[:exchange_id] = d["id"]
    out
end

parse_check_response(d::Dict) = parse_order_response(d)

parse_eid(d::Dict) = Base.get(d, "id", "None")

client = make_client(place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_check_response, parse_eid)

end # end module
