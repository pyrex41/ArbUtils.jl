function recv_json(socket::Socket)
    msg = socket |> recv |> String |> JSON.parse
end

function recv_xpub(socket::Socket)
    @assert socket.type == SUB
    msg = recv_json(socket)
    process_tick(msg)
end

function recv_string(socket::Socket)
    socket |> recv |> String
end

function get_config_x(config_socket::Socket, query)
    send(config_socket, query)
    msg = recv(config_socket) |> String |> JSON.parse
    msg[query]
end

function get_balances(exch_monitor::Socket)
    send(exch_monitor, "balances")
    indic = recv_json(exch_monitor) |> symdic
    outdic = Dict{Symbol,Dict{Symbol, Float64}}()
    for (k,v) in indic
        outdic[k] = v |> symdic
    end
    outdic
end

function setup_socket(socket_type, port::Int, ctx::Context)
    if socket_type in (REP, PUB, PUSH)
        host = "tcp://*:"
        cfunc = bind
    else
        host = "tcp://localhost:"
        cfunc = connect
    end
    socket = Socket(ctx, socket_type)
    url = string(host, port)
    cfunc(socket, url)
    if socket_type == SUB
        ZMQ.subscribe(socket, "")
    end
    socket
end

function reset_socket(in_socket::Socket)
    try
        close(in_socket)
        return qsocket(SUB, "xpub")
    catch
        return qsocket(SUB, "xpub")
    end
end
