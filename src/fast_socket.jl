function fast_update!(tick::Tick, fg::Feather, cg::FixedGraph)
    fee = cg.fees[tick.feed]
    x_sym, y_sym = tick.pair
    x = cg.graph_index[(tick.feed, x_sym)]
    y = cg.graph_index[(tick.feed, y_sym)]
    fg.time_log[y,x] = tick.time_received
    fg.time_log[x,y] = tick.time_received
    fg.bid_ask[y,x] = tick.ask
    fg.weight_matrix[y,x] = 1 / (tick.ask * (1 + fee))
    fg.bid_ask[x,y] = tick.bid
    fg.weight_matrix[x,y] = tick.bid * (1 - fee)

    #=
    if :ask in tick.flags
        fg.bid_ask[y,x] = tick.ask
        fg.weight_matrix[y,x] = 1 / (tick.ask * (1 + fee))
        #fg.time_log[y,x] = tick.time_received
    end
    if :bid in tick.flags
        fg.bid_ask[x,y] = tick.bid
        fg.weight_matrix[x,y] = tick.bid * (1 - fee)
        #fg.time_log[x,y] = tick.time_received
    end
    =#
end

function fast_list!(tick::Tick, sl::Array{Int8,1}, cg::ConstGraph)
    if :ask in tick.flags
        for c in cg.spread_lookup[(:buy, tick.feed, tick.pair)]
            sl[c] = 1
        end
    end
    if :bid in tick.flags
        for c in cg.spread_lookup[(:sell, tick.feed, tick.pair)]
            sl[c] = 1
        end
    end
end

function fast_list!(tick::Tick, f::Feather, cg::ConstGraph)
    if :ask in tick.flags
        for c in cg.spread_lookup[(:buy, tick.feed, tick.pair)]
            f.spread_list[c] = 1
        end
    end
    if :bid in tick.flags
        for c in cg.spread_lookup[(:sell, tick.feed, tick.pair)]
            f.spread_list[c] = 1
        end
    end
end

function calc!(fg::Feather, cg::ConstGraph, threshold::Float64)
    spread_holder = Array{Tuple{Int64,Float64},1}()
    fss = Array{Int64, 1}()
    for i=1:length(fg.spread_list)
        b = fg.spread_list[i]
        fg.spread_list[i] = 0
        if Bool(b)x
            push!(fss, i)
        end
    end
    @threads for c in fss
        ret = get_cycle_returns(cg.cycle_dict_all[c], fg.weight_matrix)
        if ret != Inf && ret > threshold
            push!(spread_holder, (c,ret))
        end
    end
    Base.GC.safepoint()
    spread_holder
end

function calc!(fg::Feather, cg::ConstGraph)
    spread_holder = Array{Tuple{Int64,Float64},1}()
    fss = Array{Int64, 1}()
    for i=1:length(fg.spread_list)
        b = fg.spread_list[i]
        fg.spread_list[i] = 0
        if Bool(b)
            push!(fss, i)
        end
    end
    @threads for c in fss
        ret = get_cycle_returns(cg.cycle_dict_all[c], fg.weight_matrix)
        #if ret != Inf && ret > threshold
        push!(spread_holder, (c,ret))
        #end
    end
    Base.GC.safepoint()
    spread_holder
end

function calc_n!(fg::Feather, cg::ConstGraph, threshold::Float64, n::Int64 = 10)
    spread_holder = [Array{Tuple{Int64,Float64},1}() for i in 1:nthreads()]

    @threads for c=1:length(fg.spread_list)
        b = fg.spread_list[c]
        fg.spread_list[c] = 0 # side effect
        if Bool(b)
            ret = get_cycle_returns(cg.cycle_dict_all[c], fg.weight_matrix)
            if ret != Inf && ret > threshold
                ii = threadid()
                ll = length(spread_holder[ii])

                if ll < n
                    push!(spread_holder[ii], (c,ret))
                elseif ll == n
                    sort!(spread_holder[ii], by=x->x[2])
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end
                else
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end

                    if length(spread_holder[ii]) > (2*n)
                        sort!(spread_holder[ii], by=x->x[2])
                        spread_holder[ii] = spread_holder[ii][end-n:end]
                    end
                end
            end
        end
    end
    sh = foldl(vcat, spread_holder)
    sort!(sh, by=x->x[2], rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end

function calc_n(fg::Feather, cg::ConstGraph, threshold::Float64, dlog::Dict{Int64,Bool},n::Int64=10, sn::Int64=50)
    spread_holder = [Array{Tuple{Int64,Float64},1}() for i in 1:nthreads()]
    wm_holder = [copy(fg.weight_matrix) for i in 1:nthreads()]

    @threads for c=1:length(cg.cycle_dict_all)
        if dlog[c]
            #b = fg.spread_list[c]
            #if Bool(b)
            ii = threadid()
            ret = get_cycle_returns(cg.cycle_dict_all[c], wm_holder[ii])
            if ret != Inf && ret > threshold
                ll = length(spread_holder[ii])

                if ll < n
                    push!(spread_holder[ii], (c,ret))
                elseif ll == n
                    sort!(spread_holder[ii], by=x->x[2])
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end
                else
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end

                    if length(spread_holder[ii]) > sn
                        sort!(spread_holder[ii], by=x->x[2])
                        spread_holder[ii] = spread_holder[ii][end-n:end]
                    end
                end
            end
        end
    end
    Base.GC.safepoint()
    sh = foldl(vcat, spread_holder)
    sort!(sh, by=x->x[2], rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end

function below_maxlag(max_lag::Real, ap::ArbPair, f::Feather, cg::ConstGraph)
    tn = time()
    ii = cg.cycle_dict_all[ap.id]
    for t_index in ii
        if tn - f.time_log[t_index] > max_lag
            return false
        end
    end
    true
end
below_maxlag(cg::ConstGraph, args...) = below_maxlag(args...; lookup=cg.cycle_dict_all)
below_maxlag(ng::NGraph, args...) = below_maxlag(args...; lookup=ng.cycle_list)
function below_maxlag(max_lag::Real, ap::ArbPair, f::Feather; lookup::Union{Dict,Array})
    tn = time()
    ii = lookup[ap.id]
    for t_index in ii
        if tn - f.time_log[t_index] > max_lag
            return false
        end
    end
    true
end

#function calc_revamp(f::Feather, cg::NGraph, threshold::Float64, max_lag::Real, n::Int64=10, sn::Int64=50)


function calc_n(f::Feather, cg::ConstGraph, threshold::Float64, max_lag::Real, n::Int64=10, sn::Int64=50)
    spread_holder = [ArbPair[] for i in 1:nthreads()]
    wm_holder = [copy(f.weight_matrix) for i in 1:nthreads()]

    @threads for c=1:length(cg.cycle_dict_all)
        #b = fg.spread_list[c]
        #if Bool(b)
        ii = threadid()
        ret = get_cycle_returns(cg.cycle_dict_all[c], wm_holder[ii])
        arbpair = ArbPair(c, ret)
        if ret != Inf && ret > threshold && below_maxlag(cg, max_lag, arbpair, f)
            ll = length(spread_holder[ii])

            if ll < n
                push!(spread_holder[ii], arbpair)
            elseif ll == n
                sort!(spread_holder[ii], by=value)
                if ret > spread_holder[ii][1] |> value
                    push!(spread_holder[ii], arbpair)
                end
            else
                if ret > spread_holder[ii][1] |> value
                    push!(spread_holder[ii], arbpair)
                end

                if length(spread_holder[ii]) > sn
                    sort!(spread_holder[ii], by=value)
                    spread_holder[ii] = spread_holder[ii][end-n:end]
                end
            end
        end
        #end
    end
    Base.GC.safepoint()
    sh = foldl(vcat, spread_holder)
    sort!(sh, by=value, rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end

function calc_step(i::Int64, cycle::Vector{Tuple{Int64,Int64}}, f::Feather)
    ret = get_cycle_returns(cycle, f.weight_matrix)
    arbpair = ArbPair(i,ret)
end

function calc_simple(f::Feather, ng::NGraph, threshold::Float64, max_lag::Real, n::Int64=10)
    cycle_list = ng.cycle_list
    cstep(i::Int64) = calc_step(i, cycle_list[i], f)
    myfilter(ap::ArbPair) = ap.val != Inf && ap.val > threshold && below_maxlag(ng, max_lag, ap, f)
    raw_results = 1:length(cycle_list) |> Map(cstep) |> Filter(myfilter) |> tcollect
    sort!(raw_results, by=value, rev=true)[1:n]
end

function recursive_pushsort_helper(val::ArbPair, vec::Vector{ArbPair}, i::Integer=1)
    if value(val) ≤ value(vec[i])
        i
    elseif i == length(vec)
        0
    else
        recursive_pushsort_helper(val, vec, i+1)
    end
end
function recursive_pushsort(val::ArbPair, vec::Vector{ArbPair})
    i = recursive_pushsort_helper(val, vec)
    if i == 1
        vec
    elseif i == 0
        push!(vec, val)
        vec[2:end]
    else
        vcat(vec[2:i-1], [val], vec[i:end])
    end
end

function calc_crazy(f::Feather, ng::NGraph, threshold::Float64, max_lag::Real, n::Int64=10, sn::Int64=50)
    spread_holder = [ArbPair[] for i in 1:nthreads()]
    wm_holder = [copy(f.weight_matrix) for i in 1:nthreads()]

    @threads for c=1:length(ng.cycle_list)
        #b = fg.spread_list[c]
        #if Bool(b)
        ii = threadid()
        ret = get_cycle_returns(ng.cycle_list[c], wm_holder[ii])
        arbpair = ArbPair(c, ret)
        if ret != Inf && ret > threshold && below_maxlag(ng, max_lag, arbpair, f)
            ll = length(spread_holder[ii])

            if ll < n
                push!(spread_holder[ii], arbpair)
            elseif ll == n
                spread_holder[ii] = recursive_pushsort(arbpair, spread_holder[ii])
            end
        end
    end
    Base.GC.safepoint()
    sh = foldl(vcat, spread_holder)
    sort!(sh, by=value, rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end

function calc_n(f::Feather, ng::NGraph, threshold::Float64, max_lag::Real, n::Int64=10, sn::Int64=20)
    spread_holder = [ArbPair[] for i in 1:nthreads()]
    wm_holder = [copy(f.weight_matrix) for i in 1:nthreads()]

    @threads for c=1:length(ng.cycle_list)
        #b = fg.spread_list[c]
        #if Bool(b)
        ii = threadid()
        ret = get_cycle_returns(ng.cycle_list[c], wm_holder[ii])
        arbpair = ArbPair(c, ret)
        if ret != Inf && ret > threshold && below_maxlag(ng, max_lag, arbpair, f)
            ll = length(spread_holder[ii])

            if ll < n
                push!(spread_holder[ii], arbpair)
            elseif ll == n
                sort!(spread_holder[ii], by=value)
                if ret > spread_holder[ii][1] |> value
                    push!(spread_holder[ii], arbpair)
                end
            else
                if ret > spread_holder[ii][1] |> value
                    push!(spread_holder[ii], arbpair)
                end

                if length(spread_holder[ii]) > sn
                    sort!(spread_holder[ii], by=value)
                    spread_holder[ii] = spread_holder[ii][end-n:end]
                end
            end
        end
        #end
    end
    Base.GC.safepoint()
    sh = foldl(vcat, spread_holder)
    sort!(sh, by=value, rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end


function calc_test(fg::Feather, cg::ConstGraph, threshold::Float64, max_lag::Real, n::Int64=10, sn::Int64=50)
    spread_holder = [Array{Tuple{Int64,Float64},1}() for i in 1:nthreads()]
    wm_holder = [copy(fg.weight_matrix) for i in 1:nthreads()]

    @threads for c=1:length(cg.cycle_dict_all)

            #b = fg.spread_list[c]
            #if Bool(b)
            ii = threadid()
            ret = get_cycle_returns(cg.cycle_dict_all[c], wm_holder[ii])
            if ret != Inf && ret > threshold
                ll = length(spread_holder[ii])

                if ll < n
                    push!(spread_holder[ii], (c,ret))
                elseif ll == n
                    sort!(spread_holder[ii], by=x->x[2])
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end
                else
                    if ret > spread_holder[ii][1][2]
                        push!(spread_holder[ii], (c, ret))
                    end

                    if length(spread_holder[ii]) > sn
                        sort!(spread_holder[ii], by=x->x[2])
                        spread_holder[ii] = spread_holder[ii][end-n:end]
                    end
                end
        end
    end
    Base.GC.safepoint()
    sh = foldl(vcat, spread_holder)
    filter!(sh) do (arb_id, _)
        arr = cg.cycle_dict_all[arb_id]
        time_filter(arr, fg, max_lag)
    end
    sort!(sh, by=x->x[2], rev = true)
    upb = min(length(sh), n)
    sh[1:upb]
end

function time_filter(arr::Vector{Tuple{Int64,Int64}}, f::Feather, max_lag::Real)
    t = time()
    l = length(arr)
    comp_helper(1, t, l, arr, f.time_log, max_lag)
end

function comp_helper(i::Int, t::Float64, l::Int, arr::Vector{Tuple{Int64,Int64}}, farr::SparseArrays.SparseMatrixCSC{Float64,Int64}, max_lag::Real)
    val = farr[arr[i]]
    c = t - val <= max_lag
    if i == l
        return c
    elseif c
        return comp_helper(i+1, t, l, arr, farr, max_lag)
    else
        return c
    end
end

function new_dlog(cg::ConstGraph)
    d = Dict{Int64,Bool}()
    for k in keys(cg.cycle_dict_all)
        d[k] = true
    end
    d
end

function order_info(id_pair::Tuple{Int64, Float64}, f::Feather, cg::ConstGraph, max_lag_allowed::Real, order_type::Symbol)
    id, ret = id_pair
    #f = snapshot(ff)
    tn = time()
    out = Array{Order,1}()
    hashid = hash(tn) + hash(id)
    varray = cg.cycle_dict_all[id]
    if length(varray) > 0 # added for weird error
        for vert in cg.cycle_dict_all[id]
            pi = (vert[1]=>vert[2])
            r = cg.path_index[pi]
            price_index = r[1] == :sell ? vert : reverse(vert)
            tr = f.time_log[price_index]
            o = if order_type == :limit
                LimitOrder(
                    r[2],
                    r[3],
                    r[1],
                    cg.amounts[r[3][1]],
                    f.bid_ask[price_index],
                    tn - tr,
                    tr,
                    id,
                    ret,
                    hashid
                )
            else
                MarketOrder(
                    r[2],
                    r[3],
                    r[1],
                    cg.amounts[r[3][1]],
                    f.bid_ask[price_index],
                    tn - tr,
                    tr,
                    id,
                    ret,
                    hashid
                )
            end
            push!(out, o)
        end
        max_lag = maximum(x.quote_lag for x in out)
        if max_lag  <= max_lag_allowed
            out = Dict(
                :success => true,
                :arb => ret,
                :arb_id => id,
                :orders => out,
                :max_lag => max_lag,
                :group_hash => hashid
            )
        else
            out = Dict(:success => false, :message => "exceeded max lag")
        end

    else
        out = Dict(
            :success => false
        )
    end
    out
end

function order_info(sh::Array{Tuple{Int64, Float64},1}, f::Feather, cg::ConstGraph, max_lag_allowed::Real, order_type::Symbol = :limit)
    out = Array{Dict{Symbol,Any},1}()
    for id_pair in sh
        d = order_info(id_pair, f, cg, max_lag_allowed, order_type)
        if d[:success]
            push!(out, d)
        end
    end
    out
end

function order_info_test(id_pair::Tuple{Int64, Float64}, f::Feather, cg::ConstGraph, order_type::Symbol)
    id, ret = id_pair
    #f = snapshot(ff)
    tn = time()
    out = Array{Order,1}()
    hashid = hash(tn) + hash(id)
    varray = cg.cycle_dict_all[id]
    if length(varray) > 0 # added for weird error
        for vert in cg.cycle_dict_all[id]
            pi = (vert[1]=>vert[2])
            r = cg.path_index[pi]
            price_index = r[1] == :sell ? vert : reverse(vert)
            tr = f.time_log[price_index]
            o = if order_type == :limit
                LimitOrder(
                    r[2],
                    r[3],
                    r[1],
                    cg.amounts[r[3][1]],
                    f.bid_ask[price_index],
                    tn - tr,
                    tr,
                    id,
                    ret,
                    hashid
                )
            else
                MarketOrder(
                    r[2],
                    r[3],
                    r[1],
                    cg.amounts[r[3][1]],
                    f.bid_ask[price_index],
                    tn - tr,
                    tr,
                    id,
                    ret,
                    hashid
                )
            end
            push!(out, o)
        end

        max_lag = maximum(x.quote_lag for x in out)
        #if max_lag  <= max_lag_allowed
        out = Dict(
            :success => true,
            :arb => ret,
            :arb_id => id,
            :orders => out,
            :max_lag => max_lag,
            :group_hash => hashid
        )
        #else
         #   out = Dict(:success => false, :message => "exceeded max lag")
       # end

    else
        out = Dict(
            :success => false
        )
    end
    out
end

function order_info_test(sh::Array{Tuple{Int64, Float64},1}, f::Feather, cg::ConstGraph, order_type::Symbol = :limit)
    out = Array{Dict{Symbol,Any},1}()
    for id_pair in sh
        d = order_info_test(id_pair, f, cg, order_type)
        if d[:success]
            push!(out, d)
        end
    end
    out
end


function calc_full!(fg::Feather, cg::ConstGraph, threshold::Float64)
    spread_holder = Array{Tuple{Int64,Float64, Array, Float64},1}()
    fss = Array{Int64, 1}()
    for i=1:length(fg.spread_list)
        b = fg.spread_list[i]
        fg.spread_list[i] = 0
        if Bool(b)
            push!(fss, i)
        end
    end
    for c in fss
        v_list = cg.cycle_dict_all[c]
        ret = get_cycle_returns(v_list, fg.weight_matrix)
        v_steps = Vector{Tuple}()
        for (i,z) in enumerate(cg.arb_index[c])
            (x,y) = v_list[i]
            (a,b)  = z[1] == :sell ? (x,y) : (y,x)
            price = f.bid_ask[a,b]
            timestamp = f.time_log[a,b]
            push!(v_steps, (z[1],z[2],z[3], price, timestamp))
        end
        lag = time() - minimum(x[5] for x in v_steps)
        if ret != Inf && ret > threshold
            push!(spread_holder, (c, ret, v_steps, lag))
        end
    end
    Base.GC.safepoint()
    spread_holder
end


function fast_calc!(
        spread_holder::Dict{Int64,Float64},
        tick::Tick,
        wm::SparseMatrixCSC{Float64,Int64},
        cda::Dict{Int64,Array{Tuple{Int64,Int64},1}},
        l_cda::Int,
        threshold::Float64,
        verbose::Bool = false
    )
    ccc = zeros(Int8, length(cda))
    @async if :ask in tick.flags
        @threads for c in ddd[(:sell, tick.feed, tick.pair)]
            ccc[c] = 1
        end
        Base.GC.safepoint()
    end
    @async if :bid in tick.flags
        @threads for c in ddd[(:sell, tick.feed, tick.pair)]
            ccc[c] = 1
        end
        Base.GC.safepoint()
    end
    @threads for c=1:length(ccc)
        if ccc[c] == 1
            ret = get_cycle_returns(cda[c], wm)
            if verbose
                spread_holder[c] = ret
            else
                if ret != Inf && ret > threshold
                    spread_holder[c] = ret
                else
                    delete!(spread_holder, c)
                end
            end
        end
    end
    Base.GC.safepoint()
end

"""

function fast_process!(tick::Tick, fg::Feather, cg::ConstGraph)
    fast_update!(tick, fg, cg)
    fast_list!(tick, fg, cg)
end

function format_arb_output(
        tick::Tick,
        spread_holder::Dict{Int64, Float64},
        wm::SparseMatrixCSC{Float64,Int64},
        bid_ask::SparseMatrixCSC{Float64,Int64},
        arb_index::Dict{Int64,Array{Tuple{Symbol,Symbol, Tuple{Symbol,Symbol}}}},
        cda::Dict{Int64,Array{Tuple{Int64,Int64},1}},
        t_i::Float64,
        max_lag::Float64 = 10.0
    )
    ts = time()
    out_vals = Array{Dict{Symbol,Any},1}()
    for (k,arb) in spread_holder
        orders = Array{Order,1}()
        for (i,row) in enumerate(arb_index[k])
            v1,v2 = cda[k][i]
            side, feed, pair = row
            order = Order(
                feed,
                pair,
                side,
                amounts[pair[1]],
                side == :sell ? bid_ask[v1,v2] : bid_ask[v2,v1],
                ts -  tick.timestamp,
                ts - tick.time_received
            )
            push!(orders, order)
        end

        out_dic = Dict(
            :arb_val => arb,
            :orders => orders,
            :n_orders => length(orders),
            :stale_orders => any_orders_stale(orders, max_lag)
        )
        push!(out_vals, out_dic)
    end
    sort!(out_vals, by=x->x[:arb_val], rev=true)
    tn = time()
    Dict{Symbol,Any}(
        :type => :trigger,
        :process_time =>  tn - t_i,
        :time_pushed => tn,
        :time_received => t_i,
        :order_groups => out_vals)
end

function configure_startup(config_url = "tcp://127.0.0.1:5660", initialize_graph_q = true)

    ctx = Context()
    config_query = Socket(ctx, REQ)
    ZMQ.connect(config_query, config_url)

    # inline utilities for easy config lookups
    gc(x) = get_config_x(config_query,x)
    qsocket(stype, nm) = setup_socket(stype, gc("zmq_network")[nm], ctx)

    control_socket = qsocket(REP, "arb_process_control")
    in_socket = qsocket(SUB, "xpub")
    out_socket = qsocket(PUB, "trigger_pub")
    rep_socket = qsocket(REP, "trigger_rep")

    amounts = symdic("amounts" |> gc)
    threshold = gc("arb_threshold")
    max_lag = gc("max_lag")

    # xpub socket needs to be running
    println("Building graph...")
    ag = build_graph(config_query)
    println("Graph built!")

    if initialize_graph_q
        initialize_graph(in_socket, ag)
    end

    wm = ag.weight_matrix

    rev_path_index = Dict()
    for (k,v) in ag.path_index
        if v[1] == :trade
            rev_path_index[(v[3],v[2])] = k
        end
    end

    li = []
    for (k,v) in ag.path_index
        if v[1] == :trade
            push!(li, (v[4], v[3], v[2]))
        end
    end

    ddd = Dict{Tuple{Symbol, Symbol, Tuple{Symbol, Symbol}}, Vector{Int64}}()
    for (k_arb, v_array) in ag.arbs_all
        for k in li
            if k in v_array
                d_i = get(ddd, k, Vector{Int64}())
                push!(d_i, k_arb)
                ddd[k] = d_i
            end
        end
    end

    cda = copy(ag.cycle_dict_all)
    agi = copy(ag.graph_index)
end
"""
