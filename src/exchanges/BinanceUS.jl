module BinanceUS

export client

using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, Nettle, Printf, JSON

fee_rate = .001

function req_private(method::String, path::String, query::Dict, ak::APIKey)
    method = uppercase(method)
    @assert method in ["GET", "POST", "DELETE"]

    base_url = "api.binance.us"
    path = string("/api/v3/", lstrip(path, '/'))
    t = time() * 1000
    timestamp = @sprintf "%.0f" t

    params = Dict{String, String}()
    for (k,v) in query
        params[k |> string] = v |> string
    end
    params["recvWindow"] = 5000 |> string
    params["timestamp"] = timestamp |> string

    uri = HTTP.URI(scheme="https", host=base_url, path=path, query=params)

    message = string(uri.query)

    hmac_key = String(ak.secret)
    signature = hexdigest("sha256", hmac_key, message |> String)

    q = message * "&signature=" * signature
    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "X-MBX-APIKEY"=> ak.api_key)

    if method in ("GET", "DELETE")
        uri = HTTP.URI(scheme="https", host=base_url, path=path, query=q)
        body = []
        HTTP.request(method, uri, headers=headers)
    else
        uri = HTTP.URI(scheme="https", host=base_url, path=path)
        body = q
        push!(headers, "Content-Type"=> "application/x-www-form-urlencoded")
        HTTP.request(method, uri, headers=headers, body=q)
    end
end

function req_private(method::String, path::String, ak::APIKey)
    req_private(method, path, Dict(), ak)
end

get(args...) = req_private("GET", args...)

get(path) = HTTP.request("GET", "https://api.binance.us/api/v3/"*path).body |> String |> JSON.parse

post(args...) = req_private("POST", args...)

function place_order(order::Order, ak::APIKey)
    @assert order.feed == :binanceus
    query = Dict(
        :symbol => string(order.pair[1],order.pair[2]),
        :quantity => order.amount,
        :side => order.side,
        :newClientOrderId => order.hash,
    )

    if typeof(order) == LimitOrder
        query[:type] = :limit
        query[:price] = order.price
        query[:timeInForce] = :GTC
    else
        query[:type] = :market
    end

    req_private("POST", "order", query, ak)
end

function cancel_order(sym::Tuple{Symbol,Symbol}, orderid::String, ak::APIKey)
    query = Dict(
        :symbol => string(sym[1],sym[2]),
        :orderId => orderid
    )
    req_private("DELETE", "order", query, ak)
end

function fetch_order(sym::Tuple{Symbol,Symbol}, orderID::String, ak::APIKey, clientID=false)
    id_field = clientID ? "origClientOrderId" : "orderId"
    query = Dict(
        :symbol => string(sym[1],sym[2]),
        id_field => orderID
    )
    req_private("GET", "order", query, ak)
end

function fetch_balances(ak::APIKey)
    r = req_private("GET", "account", ak) |> parse_response
    od = Array{Pair{Symbol, Float64},1}()
    for d in r["balances"]
        f = d["free"] |> safe_parse_F64
        l = d["locked"] |> safe_parse_F64
        t = f + l
        a = d["asset"] |> Symbol
        if t > 0
            push!(od, a => t)
        end
    end
    sort!(od, by=x->x.first)
end

function transfer(currency::Symbol, amt::Float64, addr::TagAddress, ak::APIKey)
    query = Dict(
        "amount" => amt,
        "asset" => currency |> string,
        "address" => addr.address,
        "addressTag" => addr.tag
    )
    req_private("POST", string("withdraw"), query, ak)
end

function transfer(currency::Symbol, amt::Float64, addr::StandardAddress, ak::APIKey)
    query = Dict(
        "amount" => amt,
        "asset" => currency |> string,
        "address" => addr.address
    )
    req_private("POST", string("withdraw"), query, ak)
end

function calculate_fees(side, price, amt)
    fee_cost = amt * fee_rate
    fee_cur = :base
    if side == :sell
        fee_cost *= price
        fee_cur = :quote
    end
    fee_cost, fee_cur
end

function parse_order_response(d::Dict)
    out = Dict{Symbol, Any}()
    s = d["status"]
    if s == "FILLED"
        out[:status] = :FILLED
        if d["type"] == "MARKET"
            out[:price_executed] = measure_market_price(d)
        end
        amt = parse(Float64, d["executedQty"])
        price = out[:price_executed]
        side = d["side"] |> lowercase |> Symbol
        fee_cost, fee_cur = calculate_fees(side::Symbol, price::Float64, amt::Float64)
        out[:fee] = fee_cost
        out[:fee_cur] = fee_cur
    else
        out[:status] = Symbol(s)
    end
    out[:exchange_id] = d["orderId"] |> string
    out
end

parse_check_response(d::Dict) = parse_order_response(d)


function measure_market_price(d::Dict)
    order_total = parse(Float64, d["cummulativeQuoteQty"])
    amt = parse(Float64, d["executedQty"])
    order_total / amt
end

function parse_eid(d::Dict)
    try
        string(d[:orderId])
    catch
        "None"
    end
end

client = make_client(place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_check_response, parse_eid)

end # end module
