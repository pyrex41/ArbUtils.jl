module ArbUtils

#export HelperUtils, OrderUtils, ClientUtils, SocketUtils, GraphUtils, ExchangeClients, Rebalancing, LogUtils

module HelperUtils
using HTTP, JSON
export reset_toggle_channel, symdic, symmap, reset_toggle_channel, get_balances, safe_parse_F64, def_parse_F64, parse_response, filter, filter!, email, clear!, stop!, precision_calc, unit_round
include("HelperUtils.jl")
end # end module

module OrderUtils
using ..HelperUtils
export Order, MarketOrder, LimitOrder, Tick, any_orders_stale, process_tick, size_check, precision_check, ArbPair, value, id
include("OrderUtils.jl")
end # end module

module SocketUtils
using ZMQ, JSON
using ..HelperUtils, ..OrderUtils
export recv_string, recv_json, recv_xpub, get_config_x, setup_socket, get_balances
include("SocketUtils.jl")
end # end module


module GraphUtils
using LightGraphs, Combinatorics, SparseArrays,ThreadsX, Base.Threads, ZMQ, Transducers
using ..HelperUtils, ..OrderUtils, ..SocketUtils
import Base.copy, Base.@lock, Base.Threads.@spawn

export ArbGraph, ExpressiveGraph, LightGraph, DynamicGraph, SnapshotGraph, Feather,
    FixedGraph, ConstGraph,NGraph, new_graph, copy, feather, lookup_constant, get_cycle_returns,
    fast_update!, fast_list!, calc!, calc_n, calc_n!, fast_process!, fast_calc!, order_info,
    lookup_price, lookup_prices, currency_convert

# abstract graph types allow extensions for specific applications that should can
# be compatible with appropriate functions
abstract type ExpressiveGraph end
abstract type LightGraph <: ExpressiveGraph end
abstract type FixedGraph end

include("structures.jl")
include("graph_structs.jl")
include("fast_socket.jl")
include("new_graph_utils.jl")
include("interact_utils.jl")
end

module ClientUtils
using ..HelperUtils, ..OrderUtils
using HTTP, JSON
export APIKey, AbstractClient, NonceClient, Client, Agent, OrderTask,ResponseTask, CheckTask, OrderStatus, AbstractOrderPlaced, OrderPlaced, OrderGO, OrderNOGO, OrderFailed, OrderUnknown, OrderState, OrderFilled, check_balance, agent_flush, agent_lookup, make_client, make_nonce_client, parse_api_keys, make_agent, process_task_helper, CryptoAddress, TagAddress, StandardAddress, parse_crypto_addresses
include("ClientUtils.jl")
end # end module

module LogUtils
using ZMQ, Redis
using ..HelperUtils, ..OrderUtils, ..ClientUtils
export format_to_save, store_order_info
include("LogUtils.jl")
end

module ExchangeClients
using JSON
using ..HelperUtils, ..OrderUtils, ..ClientUtils
export ClientDict, gather_agents, process_task, ClientChannels, getClientBalances, ClientSizeDict
include("ExchangeClients.jl")
end

module Rebalancing
using ..HelperUtils, ..OrderUtils, ..ClientUtils, ..GraphUtils, ..ExchangeClients
using LightGraphs, Printf, JSON, Transducers
import Base.@lock
export filter_convert_pairs, convert_balances, get_all_balances, percentage_total, show_rebalance, show_exchange_transfers, percentage_by_exchange, exchange_rebalance_amounts, balance_trades, shift_surplus, order_exchange_transfers, check_pairs, build_rebalance_orders, refresh_price, show_exchange_surplus, build_with_precision, tranfer, xrp_transfer, exchange_pair_graph, get_common_prices, graph_rebalance_orders, zero_unused, getvals, weightorders
include("Rebalancing.jl")
include("rebalance_graph.jl")
end # end module

#include("helper_utils.jl")
#include("test_order_utils.jl")
#include("graph_utils.jl")
#include("SFOXutils.jl")
#include("socket_funcs.jl")




end # module
