module Kraken

export client


using ...HelperUtils, ...OrderUtils, ...ClientUtils
using HTTP, Base64, Nettle, SHA, Printf
include("KrakenCurrencyConvert.jl")  # hardcoded for now

function get(path::String)
    base_url = "api.kraken.com"
    path = string("/0/public/", lstrip(path, '/'))
    uri = HTTP.URI(scheme="https", host=base_url, path=path)
    HTTP.get(uri)
end


post(args...) = req_private(args...)

function req_private(path::String, api_data::Dict, apikeys::APIKey)
    req_private(path, api_data, apikeys, time())
end

function req_private(path::String, api_data::Dict, apikeys::APIKey, t::Real)
    method = "POST"
    base_url = "api.kraken.com"

    path = string("/0/private/", lstrip(path, '/'))

    nonce = @sprintf "%.0f" t

    n_api_data = Dict{Any, Any}(
        :nonce => nonce
    )
    for (k,v) in api_data
        n_api_data[k]= v
    end

    uri = HTTP.URI(scheme="https", host=base_url, path = path, query=n_api_data)

    api_postdata = uri.query |> String
    message = string(nonce, api_postdata)
    api_256 = sha256(message)
    qq = vcat(codeunits(path), api_256)
    hmac_key = Base64.base64decode(apikeys.secret)
    api_hmacsha512 = digest("sha512", hmac_key, qq) |> Base64.base64encode

    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "API-Key"=> apikeys.api_key)
    push!(headers, "API-Sign"=> api_hmacsha512)

    uri_f = HTTP.URI(scheme="https", host=base_url, path=path)
    HTTP.request(method, uri_f, headers=headers, body = api_postdata)
end

function req_private(path::String, ak::APIKey)
    req_private(path, ak, time())
end

function req_private(path::String, ak::APIKey, t::Real)
    method = "POST"
    base_url = "api.kraken.com"

    path = string("/0/private/", lstrip(path, '/'))

    # get nonce

    nonce = @sprintf "%.0f" t

    uri = HTTP.URI(scheme="https", host=base_url, path = path, query=Dict("nonce"=>nonce))

    api_postdata = uri.query |> String
    message = string(nonce, api_postdata)
    api_256 = sha256(message)
    qq = vcat(codeunits(path), api_256)
    hmac_key = Base64.base64decode(ak.secret)
    api_hmacsha512 = digest("sha512", hmac_key, qq) |> Base64.base64encode

    headers = Array{Pair{SubString{String},SubString{String}},1}()
    push!(headers, "API-Key"=> ak.api_key)
    push!(headers, "API-Sign"=> api_hmacsha512)

    #HTTP.request(method, uri, headers=headers)
    ur1 = HTTP.request(method, string("https://",base_url,path), headers, api_postdata)
end
"""
function currency_encode(s::Symbol)
    s |> string |> currency_encode
end

function currency_encode(s::String)
    if s == "BTC"
        "XBT"
    elseif s in ["USD", "EUR", "CAD", "JPY", "GBP", "CHF", "AUD"]
        string("Z",s)
    else
        s
    end
end
"""
function currency_encode(s::Tuple{Symbol,Symbol})
    cur_convert[s]
end

function currency_encode(c::Symbol)
    scur_convert[c]
end

function place_order(order::Order, ak::APIKey, n::Real)
    @assert order.feed == :kraken
    query = Dict(
        :pair => order.pair |> currency_encode,
        :volume => order.amount,
        :type => order.side
    )
    if typeof(order) == LimitOrder
        query[:ordertype] = :limit
        query[:price] = order.price
    else
        query[:ordertype] = :market
    end

   req_private("AddOrder", query, ak, n)
end

function cancel_order(txid::String, ak::APIKey)
    query = Dict(:txid => txid)
    req_private("CancelOrder", query, ak)
end

function fetch_order(txid::String, ak::APIKey, n::Real=time()*10^3)
    query = Dict(
        :trades => "true",
        :txid => [txid]
    )
    req_private("QueryOrders", query, ak, n)
end

function fetch_order(pair::Tuple{Symbol,Symbol}, txid::String, ak::APIKey, n::Real=time()*10^3)
    fetch_order(txid,ak, n)
end

function fetch_balances(ak::APIKey, n::Real=time()*10^3)
    req_private("Balance", ak, n) |> parse_response |> parse_balances
end


function transfer(currency::Symbol, amt::Float64, addr::CryptoAddress, ak::APIKey, n::Real=time()*10^3)
    query = Dict(
        "amount" => amt,
        "key" => addr.address,
        "asset" => currency |> currency_encode
    )
    req_private("Withdraw", query, ak, n)
end

function single_cur_convert(k::String)
     if length(k) > 3 && k[1] in ('Z', 'X')
        kk = k[2:end]
         if kk == "XBT"
             return "BTC"
         else
             return kk
         end
    else
        return k
    end
end

function parse_balances(d::Dict)
    r = d["result"]
    out = Array{Pair{Symbol, Float64},1}()
    for (k,v) in r
        n = single_cur_convert(k) |> Symbol
        vv = v |> safe_parse_F64
        if vv > 0
            push!(out, n=> vv)
        end
    end
    sort!(out, by=x->x.first)
end

function parse_order_response(d::Dict)
    out = Dict{Symbol, Any}()
    if length(d["error"]) == 0
        dd = d["result"]
        for (k,v) in dd
            if typeof(v) <: Dict
                try
                    if v["status"] == "closed"
                        out[:status] = :FILLED
                        if v["descr"]["ordertype"] == "market"
                            out[:price_executed] = parse(Float64, v["price"])
                        end
                        out[:fee] = parse(Float64, v["fee"])
                        out[:fee_cur] = :quote
                    else
                        out[:status] = v["status"] |> Symbol
                    end
                    out[:exchange_id] = k
                catch
                end
            elseif k=="txid"
                out[:exchange_id] = v[1]
                out[:status] = :OPEN
            end
        end
        return out
    else
        throw(ErrorException(d["error"] |> string))
    end
end

parse_check_response(d::Dict) = parse_order_response(d)

function parse_eid(d::Dict)
    try
        arr = d["result"]["txid"]
        if length(arr) == 1
            return arr[1]
        else
            str = arr[1]
            for s in arr[2:end]
                str = string(s, "__", s)
            end
            return str
        end
    catch
        "None"
    end
end

client = make_nonce_client(time()*10^3, place_order, cancel_order, fetch_order, fetch_balances, transfer, parse_order_response, parse_check_response, parse_eid)

end # end module
