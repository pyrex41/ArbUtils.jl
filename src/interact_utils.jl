function currency_convert(amt::Real, cur1::Symbol, cur2::Symbol, ename::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock)
    pair = (cur1,cur2)
    if pair in cg.pairs_dict[ename]
        price = lookup_price(pair, ename, f, cg, l)
        unit_round(amt * price, cur2)
    else
        pair = reverse(pair)
        @assert pair in cg.pairs_dict[ename]
        price = lookup_price(pair, ename, f, cg, l)
        unit_round(amt / price, cur2)
    end
end

lookup_price(tup::Tuple{Int,Int}, f::Feather, l::ReentrantLock) = lookup_price(tup[1], tup[2], f, l)
function lookup_price(nv::Int, dv::Int, f::Feather, l::ReentrantLock)
    @lock l begin
        bid = f.bid_ask[nv,dv]
        ask = f.bid_ask[dv,nv]
        (bid+ask)/2
    end
end

function lookup_price(nv::Int, dv::Int, f::Feather)
    bid = f.bid_ask[nv,dv]
    ask = f.bid_ask[dv,nv]
    (bid+ask)/2
end


function lookup_price(pair::Tuple{Symbol,Symbol}, ename::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock)
    nv = cg.graph_index[(ename, pair[1])]
    dv = cg.graph_index[(ename, pair[2])]
    lookup_price(nv, dv, f, l)
end

function lookup_price(pair::Tuple{Symbol,Symbol}, ename::Symbol, f::Feather, cg::ConstGraph)
    nv = cg.graph_index[(ename, pair[1])]
    dv = cg.graph_index[(ename, pair[2])]
    lookup_price(nv,dv,f)
end


function lookup_price(pair::Tuple{Symbol,Symbol}, buy_sell::Symbol, ename::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock, show_lag::Bool = false)
    nv = cg.graph_index[(ename, pair[1])]
    dv = cg.graph_index[(ename, pair[2])]
    if buy_sell == :sell
        x,y = (nv,dv)
    elseif buy_sell == :buy
         x,y = [dv,nv]
    end
    price=0.0
    tlog=0.0
    @lock l begin
        price = f.bid_ask[x,y]
        tlog = f.time_log[x,y]
    end
    if show_lag
        price, tlog
    else
        price
    end
end

function lookup_prices(pairs::Array{Tuple{Symbol,Symbol}, 1}, ename::Symbol, f::Feather, cg::ConstGraph, l::ReentrantLock)
    prices = Dict{Tuple{Symbol,Symbol}, Float64}()
    @lock l begin
        for pair in pairs
            prices[pair] = lookup_price(pair, ename, f, cg)
        end
    end
    prices
end
