function exchange_pair_graph(pairs_list::Vector{Tuple{Symbol,Symbol}})
    g = SimpleDiGraph()
    graph_index = Dict{Symbol, Int64}()
    rev_index = Dict{Int64, Symbol}()
    cur_index = Vector{Symbol}()
    for (i, pair) in enumerate(pairs_list)
        push!(cur_index, pair[1])
        push!(cur_index, pair[2])
    end
    union!(cur_index)
    for (i, symb) in enumerate(cur_index)
        rev_index[i] = symb
        graph_index[symb] = i
    end

    add_vertices!(g, length(cur_index))

    for pair in pairs_list
        x = graph_index[pair[1]]
        y = graph_index[pair[2]]
        add_edge!(g, x, y)
        add_edge!(g, y, x)
    end
    g, graph_index, rev_index
end

function get_pair_list(ng::NGraph, ename::Symbol)
    myfilter(tup) = tup[1] == :buy && tup[2] == ename
    values(ng.step_dict) |> Filter(myfilter) |> Map(x->x[3]) |> collect
end
get_pair_list(cg::ConstGraph, ename::Symbol) = cg.pairs_dict[ename]


# convenience functions to quickly check prices in dollars and convert dollar_amt to quantity
function preload_dpricecheck(f::Feather, ng::NGraph, l::ReentrantLock)
    pd = Dict(k=>common_price_func(k, f, ng, l, :USD) for k in keys(ng.pairs_dict))
    (ename, cur; roundoff=true) -> roundoff ? round(pd[ename](cur), digits=2) : pd[ename](cur)
end

function common_price_func(args...; default=0.0)
    dpd = get_common_prices(args...)
    s-> get(dpd, s, ()->default)()
end
function get_common_prices(ename::Symbol, f::Feather, cg::FixedGraph, l::ReentrantLock, denom::Symbol=:USD)

    fee = cg.fees[ename]

    pairs_list = cg.pairs_dict[ename]
    g, graph_index, rev_index = exchange_pair_graph(pairs_list)

    out = Vector{Vector{Edge}}()

    function f_help(i::Int)
        c = rev_index[i]
        cg.graph_index[(ename, c)]
    end

    j = graph_index[denom]
    y = f_help(j)

    dollar_prices = Dict()

    for (s, i) in graph_index
        if s == denom
            dollar_prices[s] = () -> 1.0
        else
            if has_edge(g,i,j)
                x = i |> f_help
                dollar_prices[s] = () -> lookup_price(x,y,f,l)
            else
                edges = a_star(g,i,j)

                coords = map(edges) do edge
                    x = edge.src |> f_help
                    y = edge.dst |> f_help
                    (x,y)
                end

                #prices = zeros(length(coords))
                prices = []

                @lock l begin
                    for (i, xypair) in enumerate(coords)
                        (x,y) = xypair
                        #@inbounds prices[i] = lookup_price(x,y,f)
                        push!(prices,(x,y))
                    end
                end


                function lp(vec::Vector)
                    p = map(tup->lookup_price(tup, f, l), vec)
                    pp = zeros(Float64,length(p))
                    for (i,v) in enumerate(p)
                        if mod(i,2) == 1
                            pp[i] = v
                        else
                            pp[i] = 1 / v
                        end
                    end
                    foldl(*,pp)
                end

                dollar_prices[s] = () -> lp(prices)
            end
        end
    end
    dollar_prices
end

function getvals(pt, ename::Symbol, wtp::Dict{Symbol, <:Real}, threshold = .01)
    out = Dict{Symbol,Float64}()
    cvn = pt[:total_exchange][ename]*(1-threshold)
    for k in keys(pt[:total_currency])
        w = get(wtp, k, 0.0)
        amt = w * cvn
        out[k] = amt
    end
    out
end


function weightorders(wtp::Dict{Symbol, <:Real}, ename::Symbol,  pt::Dict{Symbol, Any}, f::Feather, cg::ConstGraph, l::ReentrantLock, threshold::Real, h::UInt64; aliases::Dict{Symbol,Symbol} = Dict{Symbol,Symbol}(), partial::Bool=false)
    if !partial
        @assert sum(values(wtp))-1.0 <= 10e-3
    end
    tgta = getvals(pt, ename, wtp)
    oo = graph_rebalance_orders(ename, pt, f, cg, l, tgta, threshold, h; aliases=aliases)
    @assert length(oo) > 0
    oo |> build_with_precision
end


function graph_rebalance_orders(ename::Symbol,  pt::Dict{Symbol, Any}, f::Feather, cg::ConstGraph, l::ReentrantLock, tgt_values::Dict{Symbol,<:Real}, threshold::Real, h::UInt64; aliases::Dict{Symbol,Symbol})

    surplus_cur = :BTC
    #=
    drc = exchange_rebalance_amounts(pt, ename, tgt_values)
    for k in keys(drc)
        if !(ename in cg.cur_exch[k])
            delete!(drc, k)
        end
    end
    =#

    out_balance, dr_balance = balance_trades(pt, ename, tgt_values, cg, threshold; aliases=aliases)

    ename_actual = Base.get(aliases, ename, ename)
    out_surplus, dr_both = shift_surplus(dr_balance, ename_actual, cg, surplus_cur, threshold)

    out = vcat(out_balance, out_surplus)

    limit_dict = Dict(:coinbasepro => [(:EOS, :BTC)])
    ll = get(limit_dict, ename, [])


    dollar_prices = get_common_prices(ename_actual, f, cg, l, :USD)

    f_help(c::Symbol) = cg.graph_index[(ename_actual, c)]

    limit_order = get(limit_dict, ename_actual, [])
    oo = map(out) do (side, pair, dollar_amt)
        x = f_help(pair[1])
        y = f_help(pair[2])
        dp = dollar_prices[pair[1]]
        price = 0.0
        qs = 0.0
        ts = time()
        @lock l begin
            if side == :sell
                price = f.bid_ask[x,y]
            else
                price = f.bid_ask[y,x]
            end
            qs = f.time_log[x,y]
        end
        ttype = pair in limit_order ? LimitOrder : MarketOrder
        amtraw = round(dollar_amt / dp, digits=8)
        ff = side == :sell ? min : max
        amt = if side == :sell
            min(amtraw,get(pt[:units_by_exchange][ename], pair[1], 0.0))
        else
            amtraw
        end
        order = ttype(
            ename,
            pair,
            side,
            amt,
            price,
            ts-qs,
            ts,
            999999,
            0.0,
            h
        )
    end
end

function zero_unused(cg::ConstGraph, ak::Dict{Symbol, APIKey})
    out = Dict()
    bb = getClientBalances(ak)
    for (ename, dict) in bb
        out[ename] = setdiff(dict |> keys |> collect, cg.cur_exch |> keys |> collect)
    end
    out
end
