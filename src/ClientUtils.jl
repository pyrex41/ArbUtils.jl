abstract type CryptoAddress end

struct TagAddress <: CryptoAddress
    address::String
    tag::String
end

struct StandardAddress <: CryptoAddress
    address::String
end

struct APIKey
    api_key::String
    secret::String
    password::String
    uid::String
end

abstract type AbstractClient end

struct Client <: AbstractClient
    place_order::Function
    cancel_order::Function
    fetch_order::Function
    fetch_balances::Function
    transfer::Function
    parse_order_response::Function
    parse_check_response::Function
    parse_eid::Function
end

mutable struct NonceClient <: AbstractClient
    nonce::Float64
    place_order::Function
    cancel_order::Function
    fetch_order::Function
    fetch_balances::Function
    transfer::Function
    parse_order_response::Function
    parse_check_response::Function
    parse_eid::Function
end

struct Agent
    feed::Symbol
    api_keys::Dict{String, String}
    place_order::Function
    cancel_order::Function
    fetch_order::Function
    fetch_balances::Function
    balances::Dict
    balances_lookup::Dict
    fee::Float64
end

abstract type OrderTask end

struct ResponseTask <: OrderTask
    task::Task
    feed::Symbol
    order::Order
    hash::UInt64
end
struct CheckTask <: OrderTask
    task::Task
    feed::Symbol
    order::Order
    hash::UInt64
    eid::String
end

abstract type OrderState end
abstract type AbstractOrderPlaced <: OrderState end
abstract type OrderGO <: OrderState end
abstract type OrderNOGO <: OrderState end

struct OrderPlaced <: AbstractOrderPlaced
    eid::String
    feed::Symbol
    order::Order
    hash::UInt64
    time::Float64
end

struct OrderStatus <: OrderGO
    hash::UInt64
    eid::String
    feed::Symbol
    order::Order
    status::Symbol
end

struct OrderFailed <: OrderNOGO
    hash::UInt64
    feed::Symbol
    order::Order
    error::Exception
    msg::Dict
end

struct OrderUnknown <: OrderNOGO
    hash::UInt64
    feed::Symbol
    order::Order
    error::Exception
    msg::Dict
end

struct OrderFilled <: OrderGO
    hash::UInt64
    eid::String
    feed::Symbol
    order::Order
    status::Symbol
    price_executed::Float64
    slippage::Float64
    fee::Float64
    fee_cur::Symbol
end




function format_to_save(o::OrderFilled)
    ut = Union{Float64, UInt64, String, Int, Symbol}
    dic = Dict{Symbol, ut}()
    dic[:arb_id] = o.order.arb_id
    dic[:feed] = o.feed
    dic[:side] = o.order.side
    dic[:hash] = o.hash
    dic[:price_executed] = o.price_executed
    dic[:price] = o.order.price
    dic[:base_cur] = o.order.pair[1]
    dic[:quote_cur] = o.order.pair[2]
    dic[:eid] = o.eid
    dic[:slippage] = o.slippage
    dic[:status] = "filled"
    dic
end

function format_to_save(o::OrderFailed)
    ut = Union{String, UInt64, Int, Symbol}
    dic = Dict{Symbol,ut}()
    dic[:arb_id] = o.order.arb_id
    dic[:hash] = o.hash
    dic[:feed] = o.feed
    dic[:side] = o.order.side
    dic[:base_cur] = o.order.pair[1]
    dic[:quote_cur] = o.order.pair[2]
    dic[:status] = "failed"
    dic
end

# modify OrderFilled to include fees

function parse_crypto_addresses(da::Dict)
    memo_list = ["XLM"]
    tag_list = ["XRP"]
    out = Dict{Symbol, Dict{Symbol, CryptoAddress}}()
    for (ename, dic) in da
        od = Dict{Symbol, CryptoAddress}()
        kk = filter(collect(keys(dic))) do k
            !occursin("_", k)
        end
        for k in kk
            addr = dic[k]
            symb = Symbol(k)
            if addr != nothing
                if k in memo_list
                    tag = dic[string(k,"_memo")]
                    od[symb] = TagAddress(addr, tag)
                elseif k in tag_list
                    tag = dic[string(k,"_tag")]
                    od[symb] = TagAddress(addr, tag)
                else
                    od[symb] = StandardAddress(addr)
                end
            end
        end
        out[Symbol(ename)] = od
    end
    out
end

function parse_api_keys(rk)
    out = Dict{Symbol, APIKey}()
    for (exch,dic) in rk
        out[Symbol(exch)] = APIKey(
            dic["apiKey"],
            get(dic, "secret", ""),
            get(dic, "password", ""),
            get(dic, "uid", "")
        )
    end
    out[:kraken] = out[:kraken_julia]
    out
end

function slippage_market_order(o::Order, ep::Float64)
    rp = (o.price - ep) * (o.side == :buy ? 1 : -1)
    unit_round(rp, o.pair[2])
end

function format_status(r::HTTP.Messages.Response, ot::CheckTask, dict_parse::Function)
    d = r.body |> String |> JSON.parse
    try
        p = dict_parse(d)
        if p[:status] == :FILLED
            out = OrderFilled(
                ot.hash,
                p[:exchange_id],
                ot.feed,
                ot.order,
                p[:status],
                p[:price_executed],
                typeof(ot.order) == MarketOrder ? slippage_market_order(ot.order, p[:price_executed]) : 0.0,
                p[:fee],
                p[:fee_cur] == :quote ? ot.order.pair[2] : ot.order.pair[1]
            )
        else
            out = OrderStatus(
                ot.hash,
                p[:exchange_id],
                ot.feed,
                ot.order,
                p[:status]
            )
        end
        return out
    catch e
        out = OrderFailed(
            ot.hash,
            ot.feed,
            ot.order,
            e,
            d
        )
        return out
    end
end

function format_status(r::HTTP.Messages.Response, ot::ResponseTask, dict_parse::Function)
    d = r.body |> String |> JSON.parse
    try
        p = dict_parse(d)
        if p[:status] == :FILLED
            out = OrderFilled(
                ot.hash,
                p[:exchange_id],
                ot.feed,
                ot.order,
                p[:status],
                p[:price_executed],
                typeof(ot.order) == MarketOrder ? slippage_market_order(ot.order, p[:price_executed]) : 0.0,
                p[:fee],
                p[:fee_cur] == :quote ? ot.order.pair[2] : ot.order.pair[1]
            )
        else
            out = OrderStatus(
                ot.hash,
                p[:exchange_id],
                ot.feed,
                ot.order,
                p[:status]
            )
        end
        return out
    catch e
        out = OrderFailed(
            ot.hash,
            ot.feed,
            ot.order,
            e,
            d
        )
        return out
    end
end

function process_task_helper(ot::OrderTask, f::Function)
    try
        return format_status(fetch(ot.task), ot, f)
    catch e
        try
            js = e.task.result.response.body |> String |> JSON.parse
            out = OrderFailed(
                ot.hash,
                ot.feed,
                ot.order,
                e,
                js
            )
            return out
        catch ee
            out = OrderUnknown(
                ot.hash,
                ot.feed,
                ot.order,
                ee,
                Dict(:msg => "Could not parse; check exception", :error1 => e)
            )
            return out
        end
    end
end

function process_task_helper(ot::ResponseTask, client::T) where {T <: AbstractClient}
    process_task_helper(ot, client.parse_order_response)
end

function process_task_helper(ot::CheckTask, client::T) where {T <: AbstractClient}
    process_task_helper(ot, client.parse_check_response)
end


function make_client(f1::Function, funcs...)
    # basic check to see if it looks like we can maybe send an order

    m = methods(f1, (Order, APIKey))
    m1 = methods(f1, (MarketOrder, APIKey))
    m2 = methods(f1, (LimitOrder, APIKey))
    @assert length(m) > 0 || (length(m1) > 0 && length(m2) > 0)

    Client(f1,funcs...)
end


function make_nonce_client(n_init::Float64, f1::Function, funcs...)
    # basic check to see if it looks like we can maybe send an order

    m = methods(f1, (Order, APIKey, Int64))
    m1 = methods(f1, (MarketOrder, APIKey, Int64))
    m2 = methods(f1, (LimitOrder, APIKey, Int64))
    @assert length(m) > 0 || (length(m1) > 0 && length(m2) > 0)

    NonceClient(n_init, f1,funcs...)
end



function make_agent(client::Client, exchange::Symbol, api_keys::Dict{String, Any}, balances::Dict{Symbol, Float64}, fee::Float64)
    f() = client.fetch_balances(api_keys)
    Agent(
        exchange,
        api_keys,
        x-> client.place_order(x, api_keys),
        x-> client.cancel_order(x, api_keys),
        x-> client.fetch_order(x, api_keys),
        f,
        deepcopy(balances),
        deepcopy(balances),
        fee
    )
end

function agent_flush(agent::Agent)
    for (sym, val) in agent.balances_lookup
        agent.balances[sym] = val
    end
    agent
end

function agent_flush(agents::Dict{Symbol,Agent})
    for (feed, agent) in agents
        agent_flush(agent)
    end
end

function agent_lookup(agent::Agent, balances_actual::Dict)
    feed = agent.feed
    for sym in keys(agent.balances)
        println(string("old balance for ", feed,":",sym,": ", agent.balances_lookup[sym]))
        new_balance = balances_actual[feed][sym]
        agent.balances_lookup[sym] = new_balance
        println(string("new balance for ", feed,":",sym,": ", agent.balances_lookup[sym]))
    end
    agent
end

function agent_lookup(agents::Dict{Symbol,Agent},balances_actual::Dict)
    for (feed, agent) in agents
        agent_lookup(agent, balances_actual)
    end
    agents
end

function calc_balance(agent::Agent, order::Order)
    base_amt = order.amount
    quote_amt = order.amount * order.price * (1+agent.fee)
    dict = Dict{Symbol, Float64}()
    (base_sign, quote_sign) = order.side == :sell ? (-1,1) : (1,-1)
    dict[order.pair[1]] = base_amt * base_sign
    dict[order.pair[2]] = quote_amt * quote_sign
    dict
end

function check_balance( order::Order, agent::Agent)
    dict = calc_balance(agent, order)
    ii = order.side == :sell ? 1 : 2
    cur_needed = order.pair[ii]
    amt_needed = dict[cur_needed] * -1
    agent.balances[cur_needed] >= amt_needed
end

function check_balance(order::Order, agents::Dict{Symbol,Agent})
    agent = agents[order.feed]
    check_balance(order, agent)
end

function check_balance(orders::Array{Order,1}, agents::Dict{Symbol,Agent})
    out = check_balance(orders[1], agents)
    ll = length(orders)
    if out && ll > 1
        out = check_balance(orders[2:end], agents)
    end
    out
end

function check_balance(od::Dict{Symbol, Any}, agents::Dict{Symbol, Agent})
    @assert :orders in keys(od)
    check_balance(od[:orders], agents)
end
