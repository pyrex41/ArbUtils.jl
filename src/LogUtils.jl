function format_to_save(o::OrderFilled)
    ut = Union{Float64, UInt64, String, Int, Symbol}
    dic = Dict{Symbol, ut}()
    dic[:arb_id] = o.order.arb_id
    dic[:feed] = o.feed
    dic[:side] = o.order.side
    dic[:hash] = o.hash
    dic[:price_executed] = o.price_executed
    dic[:price] = o.order.price
    dic[:base_cur] = o.order.pair[1]
    dic[:quote_cur] = o.order.pair[2]
    dic[:eid] = o.eid
    dic[:slippage] = o.slippage
    dic[:status] = "filled"
    dic
end

function format_to_save(o::OrderFailed)
    ut = Union{String, UInt64, Int, Symbol}
    dic = Dict{Symbol,ut}()
    dic[:arb_id] = o.order.arb_id
    dic[:hash] = o.hash
    dic[:feed] = o.feed
    dic[:side] = o.order.side
    dic[:base_cur] = o.order.pair[1]
    dic[:quote_cur] = o.order.pair[2]
    dic[:status] = "failed"
    dic
end

format_to_save(v::Vector{T}) where {T <: OrderState} = map(format_to_save, v)


function store_order_info(v::Vector{T}, socket::Socket) where {T <: OrderState}
    order_data = map(format_to_save, v)
    dic = Dict{Symbol, Any}()
    dic[:payload] = order_data

    if order_data isa Vector{OrderFilled}
        dic[:fill_status] = :success
    elseif order_data isa Vector{OrderFailed}
        dic[:fill_status] = :fail
    else
        dic[:fill_status] = :partial
    end
    dic[:arb_id] = order_data[1][:arb_id]
    dic[:hash] = order_data[1][:hash]
    dic[:type] = :order_group

    send(socket, dic |> json)
end

function store_order_info(v::Vector{T}, conn::RedisConnection) where {T <: OrderState}
    pipeline = open_pipeline(conn)

    #order_data = map(format_to_save, v)
    @assert length(union([x.hash for x in v])) == 1
    hash_ = string("0x", string(v[1].hash, base=16))
    println(hash_)
    arb_id = v[1].order.arb_id

    status = :partial
    if v isa Vector{OrderFilled}
        status = :success
    elseif v isa Vector{OrderFailed}
        status = :failure
    else
        @assert OrderFilled in union(map(typeof, order_data))
    end

    nm1 = string("tradelog:",arb_id)
    hset(pipeline, nm1, hash_, status)

    for o in v
        nm = string("tradelog:",arb_id,":", o.feed)
        dic = format_to_save(o)
        hset(pipeline, nm, dic)
    end
end
